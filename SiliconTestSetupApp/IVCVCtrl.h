#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include<stdlib.h>
//#include"termiWin.h"
#include <stdio.h>
#include <tchar.h>
#include <iomanip>
#include "prologix_gpibusb.h"
#include "keithley6517A.h"
#include "keithley2410.h"
#include "hp4192A.h"
#include "hp4284A.h"
//#include "unistd.h"
#include "pch.h"

class IVCVCtrl
{
private:
	int nNo = 99;

public:
	IVCVCtrl();
	~IVCVCtrl() {};
	int Initialize();
	int IVCVCtrl_test();
	std::string get_time_str();
	std::string get_out_format(double voltage, double current, double compliance);
	std::string get_out_format_IV(double voltage, double current);
	std::string get_out_format_IVCV(double frequency, double voltage, double current, double capacitance, double degree);
	std::string get_out_format_freq(double frequency, double capacitance, double degree);
	void clearStdin();
//	int kbhit(void);
	std::string substrback(std::string str, size_t pos, size_t len);
	int rename(std::string kind_of_exp, std::string filename_in);
	std::string read_data_file_name();
	void ReadIV(power_supply* ps, serial_interface* si);
	void ReadI(power_supply* ps, serial_interface* si);
	void ReadV(power_supply* ps, serial_interface* si);
	void ReadC(LCR_meter* lcr, serial_interface* si);
	void SetFreq(LCR_meter* lcr, serial_interface* si);
	int IVstepmeasure(power_supply* ps, serial_interface* si);
	int IVCVread(power_supply* ps, LCR_meter* lcr, serial_interface* si);
	int sweepfreq(power_supply* ps, LCR_meter* lcr, serial_interface* si);
	int sweepfreq2(power_supply* ps, LCR_meter* lcr, serial_interface* si);
	void SelectFunction();
	int CallFunction(power_supply* ps, serial_interface* si, LCR_meter* lcr);
	int main_test(serial_interface* si);
	int test(serial_interface* si);
};

