#include "pch.h"
#include "DataHandler.h"
#include <filesystem>
#include "windows.h"
#include "stdafx.h"
#include "DataHandler.h"
#include "message.h"


namespace fs = std::filesystem;

DataHandler::DataHandler()
{
	//	dirpath = "C:\\work\\Silicon\\SiliconTestSetupApp\\Data";
	fs::path path = std::filesystem::current_path();
	fs::path parent = path.parent_path();
//	std::cout << fs::absolute(parent).string() << std::endl;
	dirpath = fs::absolute(parent).string() + "\\Data\\";
	basedirpath = dirpath;
//	SetFullFilename(dirpath+"tmp.dat");
	datafilename = "tmp.dat";
	datafilenameNext = "tmp.dat";
	imagefilename = "tmp.png";
	imagefilenameNext = "tmp.png";
	MoveTmpFilesToBackup();

//	std::cout << dirpath << std::endl;
}
void DataHandler::SetQAFilename(std::string Batch, std::string Wafer, std::string Device, std::string Label, std::string Test, std::string RN)
{
	std::string pathOrigin;
	std::string path = dirpath +  Batch + "_" + Wafer + "_" + Device + "_" + Label + "_" + Test + "_" + RN + ".dat";
	pathOrigin = path;
	std::string minipath = Batch + "_" + Wafer + "_" + Device + "_" + Label + "_" + Test + "_" + RN + ".dat";
	std::ifstream ifs("C:\\Users\\atlas\\OneDrive\\Desktop\\StripQA\\QAMeasureLog.txt");
	std::string line;
	std::string StringRN = RN;
	while (getline(ifs, line)) {
		if (minipath == line) {
			int IntRN = atoi(StringRN.c_str());
			IntRN = IntRN + 1;
			StringRN = std::to_string(IntRN);
			std::cout << line << std::endl;
			if (StringRN.length() == 1) {
				StringRN = "00" + StringRN;
			}
			else if (StringRN.length() == 2) {
				StringRN = "0" + StringRN;
			}
			minipath = Batch + "_" + Wafer + "_" + Device + "_" + Label + "_" + Test + "_" + StringRN + ".dat";
			path = dirpath + Batch + "_" + Wafer + "_" + Device + "_" + Label + "_" + Test + "_" + StringRN + ".dat";
			SetQApath(minipath);
		}
	}
	std::cout << minipath << std::endl;
	ifs.close();
	std::cout << minipath << std::endl;
	std::ofstream ofs("C:\\Users\\atlas\\OneDrive\\Desktop\\StripQA\\QAMeasureLog.txt", std::ios::app);
	ofs << minipath << std::endl;
	SetFullFilename(path);
}
void DataHandler::StartNewData() {
	if (dirpath + datafilenameNext == basedirpath + "tmp.dat") {
		MoveTmpFilesToBackup();
		SetFullFilename(basedirpath + "tmp.dat");
	}
	else {
		SetFullFilename(dirpath + datafilename);
	}
}
void DataHandler::MoveTmpFilesToBackup() {
	if (!fs::exists(dirpath + "tmp.dat"))return;;
	std::string newname=DuplicationChecker(dirpath+"backup\\tmp.dat");
	fs::copy(dirpath+"tmp.dat", newname);
	fs::remove(dirpath+"tmp.dat");


	for (const auto& entry : fs::directory_iterator(dirpath)) {
		std::string ff = entry.path().string();
		if (ff.find(".png") + 4 == ff.length()) {
			std::string nff = newname.substr(0,newname.find_last_of("."));
			if (ff.find_last_of("_") != std::string::npos) {
				nff = nff + ff.substr(ff.find_last_of("_"));
			}
			else {
				nff = nff + ".png";
			}
//			std::cout << ff << " " << nff << std::endl;

			fs::copy(ff, nff);
			fs::remove(ff);
		}
	}

}

std::string DataHandler::DuplicationChecker(std::string dir, std::string file, int numlength) {
	return DuplicationChecker(dir + file,numlength);
}
std::string DataHandler::DuplicationChecker(std::string fullpath,int numlength) {
	std::string newfilename = fullpath;
	int ii = 1;
	while (fs::exists(newfilename)) {
		std::stringstream ss; ss.str("");
		ss << std::setw(numlength) << std::setfill('0') << ii ;
		newfilename = fullpath.substr(0, fullpath.find_last_of(".")) + "_" + ss.str()+ fullpath.substr(fullpath.find_last_of("."));
		ii++;
	}
	return newfilename;
}
void DataHandler::SetNextFullImageFilename() {
	fs::path filesys = DuplicationChecker(dirpath, imagefilename, 2);
	imagefilenameNext = filesys.filename().string();
}

void DataHandler::SetFilename(std::string str) {
	str = dirpath + str;
	std::cout << str << std::endl;
	SetFullFilename(str);
	/*
	str = DuplicationChecker(dirpath+str);
	if (str.find(".dat") == std::string::npos) {
		str += ".dat";
	}
	datafilename = str;
	imagefilename = datafilename.substr(0, datafilename.find_last_of(".")) + ".png";
	*/
}
void DataHandler::SetFullFilename(std::string str) {

	if (str.find(".dat") == std::string::npos) {
		str += ".dat";
	}
	fs::path filesys0 = str;
	datafilename = filesys0.filename().string();
	str = DuplicationChecker(str);

//	std::cout << str << std::endl;
	fs::path filesys = str;
	dirpath = fs::absolute(filesys.parent_path()).string() + "\\";
	datafilenameNext = filesys.filename().string();
	imagefilename = datafilenameNext.substr(0, datafilenameNext.find_last_of(".")) + ".png";
	imagefilenameNext = imagefilename;
	//	dirpath = str.substr(0, str.find_last_of("\\") + 1);
//	filename = str.substr(str.find_last_of("\\") + 2);
	MSG_INFO("Setting files : dirpath datafilename imagefilename");
	MSG_INFO(dirpath << " " << datafilenameNext << " " << imagefilename);
}
