#pragma once
#include "SiliconTestSetup.h"
#include "IVCVControlGUI.h"
#include "TUSBPIOControlGUI.h"
#include "windows.h"
#include "message.h"
//#include "number.h"
#include <iostream>
#include <sstream>
#include <string>
#include <msclr/marshal_cppstd.h>
#include <filesystem>

namespace SiliconTestSetupApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;

	/// <summary>
	/// IVCVControlGUI の概要
	/// </summary>
	public ref class IVCVControlGUI : public System::Windows::Forms::Form
	{
	public:
		IVCVControlGUI(SiliconTestSetup^ _sts)
		{
			sts = _sts;
			InitializeComponent();

		}
		IVCVControlGUI(void)
			{
				InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~IVCVControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
	private: double nowTime = 0;
	private: SiliconTestSetup^ sts;
	private: Thread^ tscan;
	private: int nIVpoints;
	private: int nCVpoints;
	private: int nCFpoints;
	private: int npreIVpoints;
	private: int npreCVpoints;
	private: int npreCFpoints;
	private: bool endArun;

	private: System::Windows::Forms::Timer^ timer1;
	private: System::Windows::Forms::ComboBox^ comboBox1;
	private: System::Windows::Forms::ComboBox^ comboBox2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::RadioButton^ radioButton1;
	private: System::Windows::Forms::RadioButton^ radioButton2;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::TextBox^ textBox9;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::Label^ label11;
	private: System::Windows::Forms::Label^ label12;
	private: System::Windows::Forms::Label^ label13;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::Label^ label15;
	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::Label^ label17;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^ chart1;
	private: System::Windows::Forms::Button^ StartButton;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::CheckBox^ checkBox1;
	private: System::Windows::Forms::CheckBox^ checkBox2;
	private: System::Windows::Forms::CheckBox^ checkBox3;
	private: System::Windows::Forms::CheckBox^ checkBox4;
	private: System::Windows::Forms::CheckBox^ checkBox5;
	private: System::Windows::Forms::Label^ label19;
	private: System::Windows::Forms::Label^ label20;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::CheckBox^ checkBox6;
	private: System::Windows::Forms::TextBox^ textBox10;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::Label^ label23;
	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::GroupBox^ groupBox4;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::RadioButton^ radioButton3;
	private: System::Windows::Forms::RadioButton^ radioButton4;
	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::Panel^ panel2;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;
	private: System::Windows::Forms::Label^ label26;
private: System::Windows::Forms::Label^ label27;
private: System::Windows::Forms::Label^ label28;
private: System::Windows::Forms::Button^ opencalib;
private: System::Windows::Forms::Button^ shortcalib;
private: System::Windows::Forms::GroupBox^ groupBox5;
private: System::Windows::Forms::Label^ label29;
private: System::Windows::Forms::Label^ label30;
private: System::Windows::Forms::TextBox^ textBox11;
private: System::Windows::Forms::TextBox^ textBox12;
private: System::Windows::Forms::Button^ button4;
private: System::Windows::Forms::Label^ label31;
private: System::Windows::Forms::Button^ button5;
private: System::Windows::Forms::Label^ label32;
private: System::Windows::Forms::Label^ label33;
private: System::Windows::Forms::TextBox^ textBox13;
private: System::Windows::Forms::TextBox^ textBox14;
private: System::Windows::Forms::TextBox^ textBox15;
private: System::Windows::Forms::GroupBox^ groupBox6;
private: System::Windows::Forms::Label^ label34;
private: System::Windows::Forms::TextBox^ textBox16;
private: System::Windows::Forms::Label^ label35;
private: System::Windows::Forms::Button^ stopButton;





		   //	private: System::Windows::Forms::CheckBox^ checkBox3;



	private: System::ComponentModel::IContainer^ components;

		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^ chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Series^ series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^ series5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->StartButton = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox5 = (gcnew System::Windows::Forms::CheckBox());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->checkBox6 = (gcnew System::Windows::Forms::CheckBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->opencalib = (gcnew System::Windows::Forms::Button());
			this->shortcalib = (gcnew System::Windows::Forms::Button());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->stopButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &IVCVControlGUI::timer1_Tick);
			// 
			// comboBox1
			// 
			this->comboBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(154, 38);
			this->comboBox1->Margin = System::Windows::Forms::Padding(2);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(182, 24);
			this->comboBox1->TabIndex = 0;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &IVCVControlGUI::comboBox1_SelectedIndexChanged_1);
			// 
			// comboBox2
			// 
			this->comboBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(154, 69);
			this->comboBox2->Margin = System::Windows::Forms::Padding(2);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(182, 24);
			this->comboBox2->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label1->Location = System::Drawing::Point(15, 40);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(125, 16);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Power supply : ";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label2->Location = System::Drawing::Point(29, 72);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(102, 16);
			this->label2->TabIndex = 3;
			this->label2->Text = L"LCR meter :";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->ForeColor = System::Drawing::Color::Blue;
			this->label3->Location = System::Drawing::Point(16, 17);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(415, 37);
			this->label3->TabIndex = 37;
			this->label3->Text = L"IV/CV measurement panel";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox1->Location = System::Drawing::Point(22, 80);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(346, 105);
			this->groupBox1->TabIndex = 38;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Select devices";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label4->Location = System::Drawing::Point(4, 8);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(131, 16);
			this->label4->TabIndex = 39;
			this->label4->Text = L"Bias Direction : ";
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->radioButton1->Location = System::Drawing::Point(148, 7);
			this->radioButton1->Margin = System::Windows::Forms::Padding(2);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(75, 20);
			this->radioButton1->TabIndex = 40;
			this->radioButton1->Text = L"Pos(+)";
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Checked = true;
			this->radioButton2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->radioButton2->Location = System::Drawing::Point(228, 8);
			this->radioButton2->Margin = System::Windows::Forms::Padding(2);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(75, 20);
			this->radioButton2->TabIndex = 41;
			this->radioButton2->TabStop = true;
			this->radioButton2->Text = L"Neg(-)";
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label5->ForeColor = System::Drawing::Color::Black;
			this->label5->Location = System::Drawing::Point(2, 42);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(170, 16);
			this->label5->TabIndex = 42;
			this->label5->Text = L"Current Limiit (uA) : ";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label6->ForeColor = System::Drawing::Color::Black;
			this->label6->Location = System::Drawing::Point(15, 76);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(157, 16);
			this->label6->TabIndex = 43;
			this->label6->Text = L"Start Voltage (V) : ";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label7->ForeColor = System::Drawing::Color::Black;
			this->label7->Location = System::Drawing::Point(22, 114);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(144, 16);
			this->label7->TabIndex = 44;
			this->label7->Text = L"End Voltage (V) : ";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label8->ForeColor = System::Drawing::Color::Black;
			this->label8->Location = System::Drawing::Point(17, 151);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(152, 16);
			this->label8->TabIndex = 45;
			this->label8->Text = L"Voltage Step (V) : ";
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox1->Location = System::Drawing::Point(176, 38);
			this->textBox1->Margin = System::Windows::Forms::Padding(2);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(76, 23);
			this->textBox1->TabIndex = 46;
			this->textBox1->Text = L"100";
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox2->Location = System::Drawing::Point(176, 73);
			this->textBox2->Margin = System::Windows::Forms::Padding(2);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(76, 23);
			this->textBox2->TabIndex = 47;
			this->textBox2->Text = L"0";
			this->textBox2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox3
			// 
			this->textBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox3->Location = System::Drawing::Point(176, 112);
			this->textBox3->Margin = System::Windows::Forms::Padding(2);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(76, 23);
			this->textBox3->TabIndex = 48;
			this->textBox3->Text = L"200";
			this->textBox3->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox4
			// 
			this->textBox4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox4->Location = System::Drawing::Point(176, 148);
			this->textBox4->Margin = System::Windows::Forms::Padding(2);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(76, 23);
			this->textBox4->TabIndex = 49;
			this->textBox4->Text = L"5";
			this->textBox4->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox5
			// 
			this->textBox5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox5->Location = System::Drawing::Point(176, 189);
			this->textBox5->Margin = System::Windows::Forms::Padding(2);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(76, 23);
			this->textBox5->TabIndex = 50;
			this->textBox5->Text = L"1";
			this->textBox5->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox6
			// 
			this->textBox6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox6->Location = System::Drawing::Point(176, 224);
			this->textBox6->Margin = System::Windows::Forms::Padding(2);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(76, 23);
			this->textBox6->TabIndex = 51;
			this->textBox6->Text = L"200";
			this->textBox6->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox7
			// 
			this->textBox7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox7->Location = System::Drawing::Point(176, 263);
			this->textBox7->Margin = System::Windows::Forms::Padding(2);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(76, 23);
			this->textBox7->TabIndex = 52;
			this->textBox7->Text = L"100";
			this->textBox7->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox8
			// 
			this->textBox8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox8->Location = System::Drawing::Point(172, 42);
			this->textBox8->Margin = System::Windows::Forms::Padding(2);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(76, 23);
			this->textBox8->TabIndex = 53;
			this->textBox8->Text = L"0.1";
			this->textBox8->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox9
			// 
			this->textBox9->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox9->Location = System::Drawing::Point(172, 83);
			this->textBox9->Margin = System::Windows::Forms::Padding(2);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(76, 23);
			this->textBox9->TabIndex = 54;
			this->textBox9->Text = L"1000";
			this->textBox9->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label9->ForeColor = System::Drawing::Color::Black;
			this->label9->Location = System::Drawing::Point(55, 190);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(108, 16);
			this->label9->TabIndex = 55;
			this->label9->Text = L"interval (s) : ";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label10->ForeColor = System::Drawing::Color::Black;
			this->label10->Location = System::Drawing::Point(8, 224);
			this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(161, 16);
			this->label10->TabIndex = 56;
			this->label10->Text = L"ramping time (ms) : ";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label11->ForeColor = System::Drawing::Color::Black;
			this->label11->Location = System::Drawing::Point(261, 41);
			this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(28, 16);
			this->label11->TabIndex = 57;
			this->label11->Text = L"uA";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label12->ForeColor = System::Drawing::Color::Black;
			this->label12->Location = System::Drawing::Point(261, 76);
			this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(19, 16);
			this->label12->TabIndex = 58;
			this->label12->Text = L"V";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label13->ForeColor = System::Drawing::Color::Black;
			this->label13->Location = System::Drawing::Point(261, 114);
			this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(19, 16);
			this->label13->TabIndex = 59;
			this->label13->Text = L"V";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label14->ForeColor = System::Drawing::Color::Black;
			this->label14->Location = System::Drawing::Point(261, 151);
			this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(19, 16);
			this->label14->TabIndex = 60;
			this->label14->Text = L"V";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label15->ForeColor = System::Drawing::Color::Black;
			this->label15->Location = System::Drawing::Point(261, 190);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(16, 16);
			this->label15->TabIndex = 61;
			this->label15->Text = L"s";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label16->ForeColor = System::Drawing::Color::Black;
			this->label16->Location = System::Drawing::Point(261, 224);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(29, 16);
			this->label16->TabIndex = 62;
			this->label16->Text = L"ms";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label17->ForeColor = System::Drawing::Color::Black;
			this->label17->Location = System::Drawing::Point(19, 265);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(149, 16);
			this->label17->TabIndex = 63;
			this->label17->Text = L"Frequency (kHz) : ";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label18->ForeColor = System::Drawing::Color::Black;
			this->label18->Location = System::Drawing::Point(261, 265);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(35, 16);
			this->label18->TabIndex = 64;
			this->label18->Text = L"kHz";
			// 
			// chart1
			// 
			chartArea1->AxisX->IsLabelAutoFit = false;
			chartArea1->AxisX->IsMarginVisible = false;
			chartArea1->AxisX->LabelStyle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			chartArea1->AxisX->MajorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
			chartArea1->AxisX->Title = L"Bias Voltage [V]";
			chartArea1->AxisX->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisY->IsLabelAutoFit = false;
			chartArea1->AxisY->IsLogarithmic = true;
			chartArea1->AxisY->LabelStyle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			chartArea1->AxisY->LabelStyle->ForeColor = System::Drawing::Color::Blue;
			chartArea1->AxisY->LineColor = System::Drawing::Color::Blue;
			chartArea1->AxisY->MajorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
			chartArea1->AxisY->Title = L"Leak Current [uA]";
			chartArea1->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisY->TitleForeColor = System::Drawing::Color::Blue;
			chartArea1->AxisY2->IsLabelAutoFit = false;
			chartArea1->AxisY2->LabelStyle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			chartArea1->AxisY2->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
			chartArea1->AxisY2->LineColor = System::Drawing::Color::DarkOrange;
			chartArea1->AxisY2->MajorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
			chartArea1->AxisY2->Title = L"Capacitance [pF]";
			chartArea1->AxisY2->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			chartArea1->AxisY2->TitleForeColor = System::Drawing::Color::DarkOrange;
			chartArea1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)));
			chartArea1->Name = L"ChartArea1";
			this->chart1->ChartAreas->Add(chartArea1);
			this->chart1->Location = System::Drawing::Point(389, 405);
			this->chart1->Margin = System::Windows::Forms::Padding(2);
			this->chart1->Name = L"chart1";
			series1->ChartArea = L"ChartArea1";
			series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series1->Legend = L"Legend1";
			series1->MarkerColor = System::Drawing::Color::Blue;
			series1->MarkerSize = 10;
			series1->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Circle;
			series1->Name = L"IVdevice1";
			series2->ChartArea = L"ChartArea1";
			series2->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series2->Legend = L"Legend1";
			series2->MarkerColor = System::Drawing::Color::DarkOrange;
			series2->MarkerSize = 10;
			series2->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Cross;
			series2->Name = L"CVdevice1";
			series2->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			series3->ChartArea = L"ChartArea1";
			series3->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			series3->Legend = L"Legend1";
			series3->MarkerColor = System::Drawing::Color::Green;
			series3->MarkerSize = 10;
			series3->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Star4;
			series3->Name = L"DVdevice1";
			series4->ChartArea = L"ChartArea1";
			series4->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			series4->Legend = L"Legend2";
			series4->MarkerColor = System::Drawing::Color::DarkOrange;
			series4->MarkerSize = 10;
			series4->Name = L"CFdevice1";
			series5->ChartArea = L"ChartArea1";
			series5->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series5->Legend = L"Legend2";
			series5->MarkerColor = System::Drawing::Color::Green;
			series5->MarkerSize = 10;
			series5->MarkerStyle = System::Windows::Forms::DataVisualization::Charting::MarkerStyle::Star5;
			series5->Name = L"DFdevice1";
			this->chart1->Series->Add(series1);
			this->chart1->Series->Add(series2);
			this->chart1->Series->Add(series3);
			this->chart1->Series->Add(series4);
			this->chart1->Series->Add(series5);
			this->chart1->Size = System::Drawing::Size(715, 557);
			this->chart1->TabIndex = 65;
			this->chart1->Text = L"chart1";
			// 
			// StartButton
			// 
			this->StartButton->BackColor = System::Drawing::Color::GreenYellow;
			this->StartButton->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 18, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->StartButton->ForeColor = System::Drawing::Color::Blue;
			this->StartButton->Location = System::Drawing::Point(824, 313);
			this->StartButton->Margin = System::Windows::Forms::Padding(2);
			this->StartButton->Name = L"StartButton";
			this->StartButton->Size = System::Drawing::Size(89, 83);
			this->StartButton->TabIndex = 66;
			this->StartButton->Text = L"Start";
			this->StartButton->UseVisualStyleBackColor = false;
			this->StartButton->Click += gcnew System::EventHandler(this, &IVCVControlGUI::startbutton_Click);
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button1->Location = System::Drawing::Point(1018, 313);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(86, 34);
			this->button1->TabIndex = 67;
			this->button1->Text = L"Clear";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &IVCVControlGUI::button1_Click);
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Checked = true;
			this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox1->Location = System::Drawing::Point(667, 324);
			this->checkBox1->Margin = System::Windows::Forms::Padding(2);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(125, 20);
			this->checkBox1->TabIndex = 68;
			this->checkBox1->Text = L"Log Scale  left";
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox1_CheckedChanged);
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox2->Location = System::Drawing::Point(667, 349);
			this->checkBox2->Margin = System::Windows::Forms::Padding(2);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(128, 20);
			this->checkBox2->TabIndex = 69;
			this->checkBox2->Text = L"Log Scale right";
			this->checkBox2->UseVisualStyleBackColor = true;
			this->checkBox2->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox2_CheckedChanged);
			// 
			// checkBox3
			// 
			this->checkBox3->AutoSize = true;
			this->checkBox3->Checked = true;
			this->checkBox3->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox3->ForeColor = System::Drawing::Color::Black;
			this->checkBox3->Location = System::Drawing::Point(26, 27);
			this->checkBox3->Margin = System::Windows::Forms::Padding(2);
			this->checkBox3->Name = L"checkBox3";
			this->checkBox3->Size = System::Drawing::Size(169, 31);
			this->checkBox3->TabIndex = 70;
			this->checkBox3->Text = L"Run IV scan";
			this->checkBox3->UseVisualStyleBackColor = true;
			this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox3_CheckedChanged);
			// 
			// checkBox4
			// 
			this->checkBox4->AutoSize = true;
			this->checkBox4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox4->ForeColor = System::Drawing::Color::Black;
			this->checkBox4->Location = System::Drawing::Point(26, 64);
			this->checkBox4->Margin = System::Windows::Forms::Padding(2);
			this->checkBox4->Name = L"checkBox4";
			this->checkBox4->Size = System::Drawing::Size(180, 31);
			this->checkBox4->TabIndex = 71;
			this->checkBox4->Text = L"Run CV scan";
			this->checkBox4->UseVisualStyleBackColor = true;
			this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox4_CheckedChanged);
			// 
			// checkBox5
			// 
			this->checkBox5->AutoSize = true;
			this->checkBox5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox5->ForeColor = System::Drawing::Color::Black;
			this->checkBox5->Location = System::Drawing::Point(26, 100);
			this->checkBox5->Margin = System::Windows::Forms::Padding(2);
			this->checkBox5->Name = L"checkBox5";
			this->checkBox5->Size = System::Drawing::Size(178, 31);
			this->checkBox5->TabIndex = 72;
			this->checkBox5->Text = L"Run CF scan";
			this->checkBox5->UseVisualStyleBackColor = true;
			this->checkBox5->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox5_CheckedChanged);
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label19->ForeColor = System::Drawing::Color::Black;
			this->label19->Location = System::Drawing::Point(16, 44);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(151, 16);
			this->label19->TabIndex = 73;
			this->label19->Text = L"Start Freq (kHz) : ";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label20->ForeColor = System::Drawing::Color::Black;
			this->label20->Location = System::Drawing::Point(24, 86);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(138, 16);
			this->label20->TabIndex = 74;
			this->label20->Text = L"End Freq (kHz) : ";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label21->ForeColor = System::Drawing::Color::Black;
			this->label21->Location = System::Drawing::Point(257, 44);
			this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(35, 16);
			this->label21->TabIndex = 75;
			this->label21->Text = L"kHz";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label22->ForeColor = System::Drawing::Color::Black;
			this->label22->Location = System::Drawing::Point(257, 86);
			this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(35, 16);
			this->label22->TabIndex = 76;
			this->label22->Text = L"kHz";
			// 
			// checkBox6
			// 
			this->checkBox6->AutoSize = true;
			this->checkBox6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->checkBox6->Location = System::Drawing::Point(667, 374);
			this->checkBox6->Margin = System::Windows::Forms::Padding(2);
			this->checkBox6->Name = L"checkBox6";
			this->checkBox6->Size = System::Drawing::Size(108, 20);
			this->checkBox6->TabIndex = 77;
			this->checkBox6->Text = L"Log Scale X";
			this->checkBox6->UseVisualStyleBackColor = true;
			this->checkBox6->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::checkBox6_CheckedChanged);
			// 
			// textBox10
			// 
			this->textBox10->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox10->Location = System::Drawing::Point(172, 129);
			this->textBox10->Margin = System::Windows::Forms::Padding(2);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(76, 23);
			this->textBox10->TabIndex = 78;
			this->textBox10->Text = L"0";
			this->textBox10->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->checkBox5);
			this->groupBox2->Controls->Add(this->checkBox4);
			this->groupBox2->Controls->Add(this->checkBox3);
			this->groupBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox2->ForeColor = System::Drawing::Color::Blue;
			this->groupBox2->Location = System::Drawing::Point(396, 232);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(200, 151);
			this->groupBox2->TabIndex = 79;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Run Mode :";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label23->ForeColor = System::Drawing::Color::Black;
			this->label23->Location = System::Drawing::Point(48, 131);
			this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(110, 16);
			this->label23->TabIndex = 80;
			this->label23->Text = L"Voltage (V) : ";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label24->ForeColor = System::Drawing::Color::Black;
			this->label24->Location = System::Drawing::Point(259, 131);
			this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(19, 16);
			this->label24->TabIndex = 81;
			this->label24->Text = L"V";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label34);
			this->groupBox3->Controls->Add(this->textBox16);
			this->groupBox3->Controls->Add(this->label35);
			this->groupBox3->Controls->Add(this->label18);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->label16);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Controls->Add(this->label14);
			this->groupBox3->Controls->Add(this->label13);
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->label11);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->textBox7);
			this->groupBox3->Controls->Add(this->textBox6);
			this->groupBox3->Controls->Add(this->textBox5);
			this->groupBox3->Controls->Add(this->textBox4);
			this->groupBox3->Controls->Add(this->textBox3);
			this->groupBox3->Controls->Add(this->textBox2);
			this->groupBox3->Controls->Add(this->textBox1);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->label7);
			this->groupBox3->Controls->Add(this->label6);
			this->groupBox3->Controls->Add(this->label5);
			this->groupBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox3->ForeColor = System::Drawing::Color::Blue;
			this->groupBox3->Location = System::Drawing::Point(29, 324);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(314, 342);
			this->groupBox3->TabIndex = 82;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"IV/CV parameters";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label34->ForeColor = System::Drawing::Color::Black;
			this->label34->Location = System::Drawing::Point(261, 304);
			this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(19, 16);
			this->label34->TabIndex = 67;
			this->label34->Text = L"V";
			// 
			// textBox16
			// 
			this->textBox16->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox16->Location = System::Drawing::Point(176, 301);
			this->textBox16->Margin = System::Windows::Forms::Padding(2);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(76, 23);
			this->textBox16->TabIndex = 66;
			this->textBox16->Text = L"1000";
			this->textBox16->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label35->ForeColor = System::Drawing::Color::Black;
			this->label35->Location = System::Drawing::Point(15, 304);
			this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(133, 16);
			this->label35->TabIndex = 65;
			this->label35->Text = L"Voltage Range : ";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label24);
			this->groupBox4->Controls->Add(this->label23);
			this->groupBox4->Controls->Add(this->textBox10);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->label20);
			this->groupBox4->Controls->Add(this->label19);
			this->groupBox4->Controls->Add(this->textBox9);
			this->groupBox4->Controls->Add(this->textBox8);
			this->groupBox4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox4->ForeColor = System::Drawing::Color::Blue;
			this->groupBox4->Location = System::Drawing::Point(32, 680);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(311, 176);
			this->groupBox4->TabIndex = 83;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"CF parameters";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label25->Location = System::Drawing::Point(10, 10);
			this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(125, 16);
			this->label25->TabIndex = 84;
			this->label25->Text = L"LCR function : ";
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Checked = true;
			this->radioButton3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->radioButton3->Location = System::Drawing::Point(148, 8);
			this->radioButton3->Margin = System::Windows::Forms::Padding(2);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(67, 20);
			this->radioButton3->TabIndex = 0;
			this->radioButton3->TabStop = true;
			this->radioButton3->Text = L"Cp-D";
			this->radioButton3->UseVisualStyleBackColor = true;
			this->radioButton3->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::radioButton3_CheckedChanged);
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->radioButton4->Location = System::Drawing::Point(228, 8);
			this->radioButton4->Margin = System::Windows::Forms::Padding(2);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(106, 20);
			this->radioButton4->TabIndex = 1;
			this->radioButton4->Text = L"Z-θ(deg)";
			this->radioButton4->UseVisualStyleBackColor = true;
			this->radioButton4->CheckedChanged += gcnew System::EventHandler(this, &IVCVControlGUI::radioButton4_CheckedChanged);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->label4);
			this->panel1->Controls->Add(this->radioButton1);
			this->panel1->Controls->Add(this->radioButton2);
			this->panel1->Location = System::Drawing::Point(22, 198);
			this->panel1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(345, 34);
			this->panel1->TabIndex = 85;
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->label25);
			this->panel2->Controls->Add(this->radioButton3);
			this->panel2->Controls->Add(this->radioButton4);
			this->panel2->Location = System::Drawing::Point(22, 242);
			this->panel2->Margin = System::Windows::Forms::Padding(2);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(346, 35);
			this->panel2->TabIndex = 86;
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button2->Location = System::Drawing::Point(1018, 361);
			this->button2->Margin = System::Windows::Forms::Padding(2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(86, 34);
			this->button2->TabIndex = 87;
			this->button2->Text = L"Save";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &IVCVControlGUI::button2_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->CheckFileExists = false;
			this->openFileDialog1->FileName = L"Folder Selection";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &IVCVControlGUI::openFileDialog1_FileOk);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button3->Location = System::Drawing::Point(806, 198);
			this->button3->Margin = System::Windows::Forms::Padding(2);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(65, 34);
			this->button3->TabIndex = 88;
			this->button3->Text = L"Set";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &IVCVControlGUI::button3_Click);
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(128)));
			this->label26->Location = System::Drawing::Point(665, 242);
			this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(80, 12);
			this->label26->TabIndex = 89;
			this->label26->Text = L"dataFilepath";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(128)));
			this->label27->Location = System::Drawing::Point(665, 265);
			this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(89, 12);
			this->label27->TabIndex = 90;
			this->label27->Text = L"imageFilepath";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label28->Location = System::Drawing::Point(629, 205);
			this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(159, 22);
			this->label28->TabIndex = 91;
			this->label28->Text = L"Save data to : ";
			// 
			// opencalib
			// 
			this->opencalib->BackColor = System::Drawing::Color::LightSkyBlue;
			this->opencalib->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->opencalib->ForeColor = System::Drawing::Color::Red;
			this->opencalib->Location = System::Drawing::Point(22, 27);
			this->opencalib->Margin = System::Windows::Forms::Padding(2);
			this->opencalib->Name = L"opencalib";
			this->opencalib->Size = System::Drawing::Size(70, 41);
			this->opencalib->TabIndex = 92;
			this->opencalib->Text = L"OPEN";
			this->opencalib->UseVisualStyleBackColor = false;
			this->opencalib->Click += gcnew System::EventHandler(this, &IVCVControlGUI::opencalib_Click);
			// 
			// shortcalib
			// 
			this->shortcalib->BackColor = System::Drawing::Color::LightSkyBlue;
			this->shortcalib->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->shortcalib->ForeColor = System::Drawing::Color::Red;
			this->shortcalib->Location = System::Drawing::Point(104, 27);
			this->shortcalib->Margin = System::Windows::Forms::Padding(2);
			this->shortcalib->Name = L"shortcalib";
			this->shortcalib->Size = System::Drawing::Size(70, 41);
			this->shortcalib->TabIndex = 93;
			this->shortcalib->Text = L"SHORT";
			this->shortcalib->UseVisualStyleBackColor = false;
			this->shortcalib->Click += gcnew System::EventHandler(this, &IVCVControlGUI::shortcalib_Click);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->shortcalib);
			this->groupBox5->Controls->Add(this->opencalib);
			this->groupBox5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox5->ForeColor = System::Drawing::Color::Blue;
			this->groupBox5->Location = System::Drawing::Point(906, 81);
			this->groupBox5->Margin = System::Windows::Forms::Padding(2);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Padding = System::Windows::Forms::Padding(2);
			this->groupBox5->Size = System::Drawing::Size(198, 96);
			this->groupBox5->TabIndex = 94;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"LCR calibration";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label29->ForeColor = System::Drawing::Color::Black;
			this->label29->Location = System::Drawing::Point(252, 35);
			this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(19, 16);
			this->label29->TabIndex = 84;
			this->label29->Text = L"V";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label30->ForeColor = System::Drawing::Color::Black;
			this->label30->Location = System::Drawing::Point(41, 35);
			this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(110, 16);
			this->label30->TabIndex = 83;
			this->label30->Text = L"Voltage (V) : ";
			// 
			// textBox11
			// 
			this->textBox11->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox11->Location = System::Drawing::Point(166, 33);
			this->textBox11->Margin = System::Windows::Forms::Padding(2);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(76, 23);
			this->textBox11->TabIndex = 82;
			this->textBox11->Text = L"-20";
			this->textBox11->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox12
			// 
			this->textBox12->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox12->Location = System::Drawing::Point(333, 35);
			this->textBox12->Margin = System::Windows::Forms::Padding(2);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(76, 23);
			this->textBox12->TabIndex = 95;
			this->textBox12->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button4->ForeColor = System::Drawing::Color::Black;
			this->button4->Location = System::Drawing::Point(284, 26);
			this->button4->Margin = System::Windows::Forms::Padding(2);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(40, 34);
			this->button4->TabIndex = 96;
			this->button4->Text = L"Set";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &IVCVControlGUI::button4_Click);
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label31->ForeColor = System::Drawing::Color::Black;
			this->label31->Location = System::Drawing::Point(417, 37);
			this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(28, 16);
			this->label31->TabIndex = 65;
			this->label31->Text = L"uA";
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button5->ForeColor = System::Drawing::Color::Black;
			this->button5->Location = System::Drawing::Point(284, 66);
			this->button5->Margin = System::Windows::Forms::Padding(2);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(40, 34);
			this->button5->TabIndex = 97;
			this->button5->Text = L"Set";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &IVCVControlGUI::button5_Click);
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label32->ForeColor = System::Drawing::Color::Black;
			this->label32->Location = System::Drawing::Point(250, 75);
			this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(35, 16);
			this->label32->TabIndex = 67;
			this->label32->Text = L"kHz";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label33->ForeColor = System::Drawing::Color::Black;
			this->label33->Location = System::Drawing::Point(8, 75);
			this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(149, 16);
			this->label33->TabIndex = 66;
			this->label33->Text = L"Frequency (kHz) : ";
			// 
			// textBox13
			// 
			this->textBox13->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox13->Location = System::Drawing::Point(165, 73);
			this->textBox13->Margin = System::Windows::Forms::Padding(2);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(76, 23);
			this->textBox13->TabIndex = 65;
			this->textBox13->Text = L"100";
			this->textBox13->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox14
			// 
			this->textBox14->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox14->Location = System::Drawing::Point(333, 73);
			this->textBox14->Margin = System::Windows::Forms::Padding(2);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(76, 23);
			this->textBox14->TabIndex = 98;
			this->textBox14->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// textBox15
			// 
			this->textBox15->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox15->Location = System::Drawing::Point(419, 73);
			this->textBox15->Margin = System::Windows::Forms::Padding(2);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(76, 23);
			this->textBox15->TabIndex = 99;
			this->textBox15->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->textBox15);
			this->groupBox6->Controls->Add(this->textBox14);
			this->groupBox6->Controls->Add(this->label32);
			this->groupBox6->Controls->Add(this->button5);
			this->groupBox6->Controls->Add(this->label33);
			this->groupBox6->Controls->Add(this->textBox13);
			this->groupBox6->Controls->Add(this->label31);
			this->groupBox6->Controls->Add(this->button4);
			this->groupBox6->Controls->Add(this->textBox12);
			this->groupBox6->Controls->Add(this->label29);
			this->groupBox6->Controls->Add(this->label30);
			this->groupBox6->Controls->Add(this->textBox11);
			this->groupBox6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox6->ForeColor = System::Drawing::Color::Blue;
			this->groupBox6->Location = System::Drawing::Point(386, 64);
			this->groupBox6->Margin = System::Windows::Forms::Padding(2);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Padding = System::Windows::Forms::Padding(2);
			this->groupBox6->Size = System::Drawing::Size(506, 121);
			this->groupBox6->TabIndex = 100;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Single point test";
			// 
			// stopButton
			// 
			this->stopButton->BackColor = System::Drawing::Color::MediumVioletRed;
			this->stopButton->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 18, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->stopButton->ForeColor = System::Drawing::Color::Cyan;
			this->stopButton->Location = System::Drawing::Point(917, 314);
			this->stopButton->Margin = System::Windows::Forms::Padding(2);
			this->stopButton->Name = L"stopButton";
			this->stopButton->Size = System::Drawing::Size(89, 83);
			this->stopButton->TabIndex = 101;
			this->stopButton->Text = L"Stop";
			this->stopButton->UseVisualStyleBackColor = false;
			this->stopButton->Click += gcnew System::EventHandler(this, &IVCVControlGUI::stopButton_Click);
			// 
			// IVCVControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1129, 1001);
			this->Controls->Add(this->stopButton);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->label28);
			this->Controls->Add(this->label27);
			this->Controls->Add(this->label26);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->checkBox6);
			this->Controls->Add(this->checkBox2);
			this->Controls->Add(this->checkBox1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->StartButton);
			this->Controls->Add(this->chart1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->groupBox1);
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"IVCVControlGUI";
			this->Text = L"IVCVControlGUI";
			this->Load += gcnew System::EventHandler(this, &IVCVControlGUI::IVCVControlGUI_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

		// ********* Above lines must not be editted via source! ********

		void RunScan() {

//			nIVpoints = 0;
//			nCVpoints = 0;
			bool isNegativeVoltage=true;
			if (this->radioButton1->Checked)isNegativeVoltage = false;
			if (this->radioButton2->Checked)isNegativeVoltage = true;
			sts->m_isNegativeVoltage = isNegativeVoltage;
			sts->m_currentLimit = fabs(float::Parse(this->textBox1->Text));
			sts->m_Vstart       = fabs(float::Parse(this->textBox2->Text));
			sts->m_Vend         = fabs(float::Parse(this->textBox3->Text));
			sts->m_Vstep        = fabs(float::Parse(this->textBox4->Text));
			sts->m_interval     = fabs(float::Parse(this->textBox5->Text));
			sts->m_Tramp        = fabs(float::Parse(this->textBox6->Text));
			sts->m_F            = fabs(float::Parse(this->textBox7->Text));
			sts->m_Fstart       = fabs(float::Parse(this->textBox8->Text));
			sts->m_Fend         = fabs(float::Parse(this->textBox9->Text));
			sts->m_Vfixed       = fabs(float::Parse(this->textBox10->Text));
			sts->m_Vrange       = fabs(float::Parse(this->textBox16->Text));

			MSG_INFO("all parameter have been set ");
			if (!checkBox5->Checked) {
				MSG_INFO(" running IV or CV scan ");
				sts->runIVCVScan(checkBox3->Checked, checkBox4->Checked);
			}
			else {
				MSG_INFO(" running C measurement as a function of Frequency");
				sts->runCFScan();
			}
			endArun = true;

		}
		void ClearPlots() {
//			SaveImageToFile();

			this->chart1->ChartAreas[L"ChartArea1"]->AxisX->IsLogarithmic = false;
			this->checkBox6->Checked = false;

			if (nIVpoints !=0 ) this->chart1->Series["IVdevice1"]->Points->Clear();
			if (nCVpoints != 0) {
				this->chart1->Series["CVdevice1"]->Points->Clear();
				this->chart1->Series["DVdevice1"]->Points->Clear();
			}
			if (nCFpoints != 0) {
				this->chart1->Series["CFdevice1"]->Points->Clear();
				this->chart1->Series["DFdevice1"]->Points->Clear();
			}
			sts->clearData();
		}
		void PlottingDemo() {

			for(int ii=0;ii<200;ii++){
				double V = (double)ii;
				System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint1 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(V, sqrt(0.1+V)));
				System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint2 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(V, 1/(1+sqrt(V))));
				this->chart1->Series["IVdevice1"]->Points->Add(tmppoint1);
				this->chart1->Series["CVdevice1"]->Points->Add(tmppoint2);
				Sleep(10);
			}

		}
		
		void PlotValue(std::vector<double> vol1, std::vector<double> curr, std::vector<double> vol2, std::vector<double>cap, std::vector<double>deg, std::vector<double>freq) {
			//if (vol1.size() == 0 && vol2.size() == 0)return;
			if (vol1.size() > 0) {
				for (unsigned int ii = 0; ii < vol1.size(); ii++) {
//					if (this->chart1->ChartAreas[L"ChartArea1"]->AxisX->IsLogarithmic == true) {
//						if (vol1[ii] == 0)vol1[ii] = 1e-20;
//						if (vol1[ii] < 0)vol1[ii] = fabs(vol1[ii]);
//					}
//					if (this->chart1->ChartAreas[L"ChartArea1"]->AxisY->IsLogarithmic == true) {
//						if (vol1[ii] == 0)cur1[ii] = 1e-20;
//						if (vol1[ii] < 0)cur1[ii] = fabs(cur1[ii]);
//					}
					System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint1 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(vol1[ii], curr[ii]));
					this->chart1->Series["IVdevice1"]->Points->Add(tmppoint1);
				}
			}
			if (vol2.size() > 0) {
				for (unsigned int ii = 0; ii < vol2.size(); ii++) {
					System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint2 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(vol2[ii], cap[ii]));
					this->chart1->Series["CVdevice1"]->Points->Add(tmppoint2);
				}
				if (vol1.size() == 0) {
					for (unsigned int ii = 0; ii < vol2.size(); ii++) {
						System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint3 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(vol2[ii], deg[ii]));
						this->chart1->Series["DVdevice1"]->Points->Add(tmppoint3);
					}
				}

			}
			if (freq.size() > 0) {
				for (unsigned int ii = 0; ii < freq.size(); ii++) {
					System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint4 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(freq[ii], cap[ii]));
					this->chart1->Series["CFdevice1"]->Points->Add(tmppoint4);
					System::Windows::Forms::DataVisualization::Charting::DataPoint^ tmppoint5 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(freq[ii], deg[ii]));
					this->chart1->Series["DFdevice1"]->Points->Add(tmppoint5);
				}
			}
		}
		void SetAxisTo(System::Windows::Forms::DataVisualization::Charting::Axis^ xaxis, System::Windows::Forms::DataVisualization::Charting::Axis^ yaxis, int xtype, int ytype) {
			if (xtype == 0) {
				xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
				xaxis->LineColor = System::Drawing::Color::Black;
				xaxis->Title = L"Bias Voltage [V]";
				xaxis->TitleForeColor = System::Drawing::Color::Black;
				xaxis->Minimum = 0;
//				xaxis->IsLogarithmic = false;

			}
			else if(xtype == 1) {
				xaxis->LabelStyle->ForeColor = System::Drawing::Color::Black;
				xaxis->LineColor = System::Drawing::Color::Black;
				xaxis->Title = L"Frequency [kHz]";
				xaxis->TitleForeColor = System::Drawing::Color::Black;
//				xaxis->IsLogarithmic = true;
				xaxis->Minimum = 0;

			}
			else {
				MSG_ERROR("strange axis type set " << xtype);
			}
			if (ytype == 0) {
				yaxis->LabelStyle->ForeColor = System::Drawing::Color::Blue;
				yaxis->LineColor = System::Drawing::Color::Blue;
				yaxis->Title = L"Leak Current [uA]";
				yaxis->TitleForeColor = System::Drawing::Color::Blue;

			}
			else if (ytype == 1) {
				yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
				yaxis->LineColor = System::Drawing::Color::DarkOrange;
				yaxis->Title = L"Capacitance [pF]";
				yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;

			}
			else if (ytype == 2) {
				yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkGreen;
				yaxis->LineColor = System::Drawing::Color::DarkGreen;
				yaxis->Title = L"D Factor ";
				yaxis->TitleForeColor = System::Drawing::Color::DarkGreen;

			}
			else if (ytype == 3) {
				yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkOrange;
				yaxis->LineColor = System::Drawing::Color::DarkOrange;
				yaxis->Title = L"Impedance [kΩ]";
				yaxis->TitleForeColor = System::Drawing::Color::DarkOrange;

			}
			else if (ytype == 4) {
				yaxis->LabelStyle->ForeColor = System::Drawing::Color::DarkGreen;
				yaxis->LineColor = System::Drawing::Color::DarkGreen;
				yaxis->Title = L"Phase Angle [deg] ";
				yaxis->TitleForeColor = System::Drawing::Color::DarkGreen;

			}
			else {
				MSG_ERROR("strange axis type set " << ytype);
			}




		}
		private: void SaveImageToFile() {
			System::String^ filename = gcnew System::String((sts->getDataHandler()->GetFullImageFilename()).c_str());
			this->chart1->SaveImage(filename, System::Windows::Forms::DataVisualization::Charting::ChartImageFormat::Png);
			sts->getDataHandler()->SetNextFullImageFilename();
			ShowFilename();
		}
		private: void ShowFilename() {
			String^ fn = gcnew String(sts->getDataHandler()->GetFullDataFilename().c_str());
			label26->Text = fn;
			String^ fni = gcnew String(sts->getDataHandler()->GetFullImageFilename().c_str());
			label27->Text = fni;

		}
#pragma endregion

	private: System::Void stopButton_Click(System::Object^ sender, System::EventArgs^ e) {
		MSG_INFO("stop buton pushed");
		tscan->Abort();
		delete tscan;
		sts->stopRun();
	}
	private: System::Void startbutton_Click(System::Object^ sender, System::EventArgs^ e) {
//		PlottingDemo();
//		RunScan();
//		return;
		MSG_INFO("====  starting run ==== ");

		ClearPlots();
		sts->getDataHandler()->StartNewData();
		ShowFilename();
		int Atype = 0;
		int Btype = 0;
		if (this->radioButton3->Checked == true) {
			Atype = 1;
			Btype = 2;
			sts->setLCRFuncType(0);

		}
		else if (this->radioButton4->Checked == true) {
			Atype = 3;
			Btype = 4;
			sts->setLCRFuncType(1);
		}

		if (checkBox3->Checked) {
			this->chart1->Series["IVdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
			SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY,0,0);
			if (checkBox4->Checked) {
				this->chart1->Series["CVdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
				SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY2, 0, Atype);

			}
		}
		else if (checkBox4->Checked) {
			this->chart1->Series["CVdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
			SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 0, Atype);
			this->chart1->Series["DVdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY2, 0, Btype);

		}
		else if (checkBox5->Checked) {
			this->chart1->Series["CFdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Primary;
			SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY, 1, Atype);
			this->chart1->Series["DFdevice1"]->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
			SetAxisTo(this->chart1->ChartAreas[L"ChartArea1"]->AxisX, this->chart1->ChartAreas[L"ChartArea1"]->AxisY2, 1, Btype);
		}

		ThreadStart^ threadDelegate = gcnew ThreadStart(this, &IVCVControlGUI::RunScan);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();

	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
		SaveImageToFile();

	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
		saveFileDialog1->ShowDialog();
		String^ datafilename = saveFileDialog1->FileName;

		sts->getDataHandler()->SetFullFilename(msclr::interop::marshal_as<std::string>(saveFileDialog1->FileName));
		ShowFilename();
		MSG_INFO(sts->getDataHandler()->GetFullDataFilename());
		MSG_INFO(sts->getDataHandler()->GetFullImageFilename());

	}
	private: System::Void radioButton3_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		sts->setLCRFuncType(0);
	}
	private: System::Void radioButton4_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		sts->setLCRFuncType(1);
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		// Clear button click
		SaveImageToFile();
		ClearPlots();
	}


	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
		// single point voltage set
		bool isNegative = true;
		if (this->radioButton1->Checked)isNegative = false;
		if (this->radioButton2->Checked)isNegative = true;
		sts->m_Vsingle = (isNegative ? -1 : +1) * fabs(float::Parse(this->textBox11->Text));;
		float cur = -1;
		sts->readSingleCurrent(cur);
		MSG_INFO(cur / uA);
		std::string scur = std::to_string(cur/uA);
		String^ singlecur =gcnew String(scur.c_str())	;
		textBox12->Text = singlecur ;

	}
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
		// single point impedance set
		sts->m_Fsingle = fabs(float::Parse(this->textBox13->Text));;
		float cap = -1, deg = -1;
		sts->readSingleCorZ(cap, deg, SiliconTestSetup::LCRFuncType::Cp_D);
		MSG_INFO(cap << " " << deg);
		std::string scap = std::to_string(cap);
		String^ singlecap = gcnew String(scap.c_str());
		std::string sdeg = std::to_string(deg);
		String^ singledeg = gcnew String(sdeg.c_str());
		textBox14->Text = singlecap;
		textBox15->Text = singledeg;
	}

	private: System::Void opencalib_Click(System::Object^ sender, System::EventArgs^ e) {
		// open calibration
		sts->doOpenCalibration();
	}
	private: System::Void shortcalib_Click(System::Object^ sender, System::EventArgs^ e) {
		// short calibration
		sts->doShortCalibration();

	}

	private: System::Void checkBox1_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (this->checkBox1->Checked == true) {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisY->IsLogarithmic = true;
		}
		else {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisY->IsLogarithmic = false;
		}

	}
	private: System::Void checkBox2_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (this->checkBox2->Checked == true) {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisY2->IsLogarithmic = true;
		}
		else {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisY2->IsLogarithmic = false;
		}
	}
	private: System::Void checkBox6_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (this->checkBox6->Checked == true) {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisX->IsLogarithmic = true;
			this->chart1->ChartAreas[L"ChartArea1"]->AxisX->Minimum=0.1;
		}
		else {
			this->chart1->ChartAreas[L"ChartArea1"]->AxisX->IsLogarithmic = false;
			this->chart1->ChartAreas[L"ChartArea1"]->AxisX->Minimum=0.0;

		}

	}
	private: System::Void checkBox3_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (checkBox3->Checked == true) {
			checkBox5->Checked = false;
		}
	}
	private: System::Void checkBox4_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (checkBox4->Checked == true) {
			checkBox5->Checked = false;
		}
	}
	private: System::Void checkBox5_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
		if (checkBox5->Checked == true) {
			checkBox3->Checked = false;
			checkBox4->Checked = false;
		}
	}
	private: System::Void IVCVControlGUI_Load(System::Object^ sender, System::EventArgs^ e) {
		bool setps = false;
		bool setlcr = false;
		MSG_INFO(sts->getPS()->size() << "  power supply added...");
		cli::array< System::Object^  >^ ary1 = gcnew cli::array<System::Object^>(sts->getPS()->size());
		int ii = 0;
		for (auto xx : *(sts->getPS())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address();
			ary1[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary1[ii++]);
		}
		this->comboBox1->Items->AddRange(ary1);
		if (sts->getPS()->size() != 0) {
			this->comboBox1->Text = (String^)ary1[0];
			setps = true;
		}
		MSG_INFO(sts->getLCR()->size() << "  LCR meter added...");
		cli::array< System::Object^  >^ ary2 = gcnew cli::array<System::Object^>(sts->getLCR()->size());
		ii = 0;
		for (auto xx : *(sts->getLCR())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address() ;
			ary2[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary2[ii++]);
		}
		this->comboBox2->Items->AddRange(ary2);
		if (sts->getLCR()->size() != 0) {
			this->comboBox2->Text = (String^)ary2[0];
			setlcr = true;
		}
		/*
		for (auto xx : *(sts->getLCR())) {
			std::cout << xx.first << " " << xx.second->get_address() << std::endl;
		*/
		if (sts->getPS()->size() == 0 && sts->getLCR()->size() == 0) {
			String^ mess_str = L"No device found.  Please select device in Control Panel.";
			System::Windows::Forms::MessageBox::Show(mess_str);
		}
		String^ psdev;
		String^ lcrdev;
		if (setps) {
			psdev = (String^)ary1[0];
			checkBox3->Checked = true;
			if (setlcr) {
				lcrdev = (String^)ary2[0];
				checkBox4->Checked = true;
			}
			else {
				lcrdev = L"";
				checkBox4->Checked = false;
			}
		}
		else {
			psdev = L"";
			checkBox3->Checked = false;
			checkBox4->Checked = false;
			if (setlcr) {
				lcrdev = (String^)ary2[0];
				checkBox5->Checked = true;
			}

		}
//		sts->getDataHandler()->MoveTmpFilesToBackup();
		ShowFilename();
		endArun = false;

		sts->setCurrentActiveDevice(msclr::interop::marshal_as<std::string>(psdev), msclr::interop::marshal_as<std::string>(lcrdev));
		npreIVpoints = 0;
		npreCVpoints = 0;
		npreCFpoints = 0;
		timer1->Start();
	}

	private: System::Void comboBox1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		sts->setCurrentActiveDevice(msclr::interop::marshal_as<std::string>((String^)comboBox1->Text), "");
	}
	private: System::Void comboBox2_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		sts->setCurrentActiveDevice("",msclr::interop::marshal_as<std::string>((String^)comboBox2->Text));
	}
	private: System::Void timer1_Tick(System::Object^ sender, System::EventArgs^ e) {
		nIVpoints = sts->getVolt1().size();
		nCVpoints = sts->getVolt2().size();
		nCFpoints = sts->getFrequency().size();
		if (nCVpoints != npreCVpoints || nIVpoints != npreIVpoints || nCFpoints != npreCFpoints) {
			PlotValue(sts->getVolt1(), sts->getCurrent(), sts->getVolt2(),sts->getCapacitance(),sts->getDegree(),sts->getFrequency());
			npreIVpoints = nIVpoints;
			npreCVpoints = nCVpoints;
			npreCFpoints = nCFpoints;
		}
		if (endArun == true) {
			SaveImageToFile();
			endArun = false;
		}

		nowTime = nowTime + 0.05;
	}







private: System::Void openFileDialog1_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {
}
private: System::Void comboBox1_SelectedIndexChanged_1(System::Object^ sender, System::EventArgs^ e) {
}
};
}
