#pragma once
#include "ComPort.h"
class PrologixGPIB :
	 public ComPort
{
private:
	std::string port;
	std::string  cmd;
	DWORD error = 0;
	const DWORD TIMEOUT = 3000;     // Millisec
	static const unsigned int MAX_READ = 4096; // 4096;

	bool initialized;
public:
	PrologixGPIB();
	PrologixGPIB(std::string _port) {
		PrologixGPIB();
		SetComPortNumber(_port)	;
	}
	~PrologixGPIB() {

	}
	int initialize();
	int initialize(std::string _port) {
		SetComPortNumber(_port);
		return initialize();
	}
	int finalize();
	int ClosePort();
	void SetComPortNumber(std::string _port) { port = _port; }

	bool is_initialized() { return initialized; };
	int writeread(std::string _cmd, std::string& buf);
	int write(char* buf, size_t length);
	int write(std::string bufstr);
	int read(char* buf, size_t length);
	int read(std::string& buf);

};

