#pragma once

#include <windows.h>
#include <iostream>
#include "prologix_gpibusb.h"
#include "IVCVCtrl.h"
#include "TUSBPIOCtrl.h"
#include "TeslaProbeCtrl.h"
#include "DataHandler.h"
#include "message.h"
#include <vector>
#include <map>
#include <string>

#define uA 1e-6
#define kHz 1e3
#define pF 1e-12
#define kOhm 1e3

constexpr auto DEVICE_NAME = "COM4";

ref class SiliconTestSetup
{
private:
	DataHandler* m_dataHandler;
	serial_interface* m_serialInterface;
	TeslaProbeCtrl* m_teslaProbeCtrl;
	TUSBPIOCtrl* m_usbpio;
	IVCVCtrl* m_ivcv;

	std::vector<double>* m_curr;
	std::vector<double>* m_curr2;
	std::vector<double>* m_curr3;
	std::vector<double>* m_cap; // capacitance 
	std::vector<double>* m_deg;
	std::vector<double>* m_res; //resistance
	std::vector<double>* m_freq;
	std::vector<double>* m_Dfactor;
	std::vector<double>* m_Resistance;

	std::vector<double>* m_vol1;
	std::vector<double>* m_vol2;

	
	bool m_isGPIBOpen;
	std::map<power_supply*, std::string>* m_powerSupplySetting_Map;
	std::map<LCR_meter*, std::string>* m_LCRMeterSetting_Map;

	// active device for simple measurement
	// std::string needs to be defined as a pointer due to a feature of the garbage collection (?)
	power_supply* m_activePS;
	std::string* m_activePS_name;
	LCR_meter* m_activeLCR;
	std::string* m_activeLCR_name;

	// active device for strip QA pieces
	// std::string needs to be defined as a pointer due to a feature of the garbage collection (?)
	power_supply* m_activePSHV;
	std::string* m_activePSHV_name;
	power_supply* m_activePSVtest;
	std::string* m_activePSVtest_name;
	LCR_meter* m_activeLCR1;
	std::string* m_activeLCR1_name;
	LCR_meter* m_activeLCR2;
	std::string* m_activeLCR2_name;

	// run type for IV/CV measurement
	bool m_doneIV;
	bool m_doneCV;
	bool m_doneCF;

public:
	SiliconTestSetup();
	~SiliconTestSetup() {}

	// device setting
	bool  m_isNegativeVoltage;
	float m_currentLimit;
	float m_interval;

	// V = voltage
	// T = time
	// F = frequency
	float m_Tramp;
	float m_Vstart;
	float m_Vend;
	float m_Vend_HV;
	float m_Vstep;
	float m_Vfixed;
	float m_Vsingle;
	float m_Vrange;
	float m_F;
	float m_Fstart;
	float m_Fend;
	float m_Fsingle;

	// stripQA general settings
	enum class TestTypeIdentifier { EMPTY, RBIAS_IV, CCPL_C, INT_LOW_C, INT_MID_IV, PTP_IV, CCPL_IV_10, CFLD_CV, MD8_IVCV };
	TestTypeIdentifier m_QAparam_testTypeIndex;
	enum class LCRFuncType { Cp_D, Z_theta, Cs_D, NOT_SET};
	LCRFuncType m_LCRFuncType; // 0: Cp_D ,   1: Z_theta ,  2: Cs_D, 3: NOT_SET is used only for initialisation in the constructor.
	

	System::String^ m_QAparam_waferType; //Wafar Type
	String^ m_QAparam_batchNumber; //Batch Number
	String^ m_QAparam_waferNumber; //Wafer Number
	String^ m_QAparam_DeviceType;
	String^ m_QAparam_DeviceNumber;
	String^ m_QAparam_label;
	String^ m_QAparam_serialNumber; //Component Serial Number
	String^ m_QAparam_operatorName; //Operator Name
	String^ m_QAparam_testType; //Test type
	String^ m_QAparam_deviceType; //Device Type
	String^ m_QAparam_runNumber;
	String^ m_QAparam_temperature; //Temperature -C
	String^ m_QAparam_humidity; //Humidity

	int initialize();
	std::string getCurrentTime();
	TUSBPIOCtrl* getTUSBPIO() { return m_usbpio; }
	void legacyRun();
	bool getGPIBStatus() {return m_isGPIBOpen;}
	DataHandler* getDataHandler() {return m_dataHandler; }
	int openApplication(char* szCmd);
	int openTeslaT200Control();
	int createNewConsole(char* szCmd);
	int setPS(std::string PSname, int addr);
	int setLCR(std::string LCRname, int addr);
	int removePS(std::string PSname, int addr);
	int removeLCR(std::string LCRname, int addr);
	std::vector<double> getVolt1() { return *m_vol1; }
	std::vector<double> getCurrent() { return *m_curr; }
	std::vector<double> getCurrent1() { return *m_curr; }
	std::vector<double> getCurrent2() { return *m_curr2; }
	std::vector<double> getCurrent3() { return *m_curr3; }
	std::vector<double> getVolt2() { return *m_vol2; }
	std::vector<double> getResistance() { return *m_res; }
	std::vector<double> getCapacitance() { return *m_cap; }
	std::vector<double> getDegree() { return *m_deg; }
	std::vector<double> getFrequency() { return *m_freq; }
	void doCalibration(bool isOpen);
	void doOpenCalibration();
	void doShortCalibration();
	void turnPSOn(power_supply* PS, float Ilimit);
	void turnPSOn(power_supply* PS, float Ilimit, bool setVrange);
	void rampPSVoltage(power_supply* PS, float Vset, int Twait);
	void getPSCurrent(power_supply* PS, float &Imeas);
	void stopPS(power_supply* PS);
	//void setLCR(LCR_meter* LCR);
	void resetLCR(LCR_meter* LCR);
	void resetLCR(LCR_meter* LCR, LCRFuncType funcType);
	void changeLCRFrequency(LCR_meter* LCR, float Fset, int Twait);
	void getLCRValues(LCR_meter* LCR, float& C_or_Z, float& D_or_theta);
	void stopLCR(LCR_meter* LCR);
	void setLCRFuncType(int type) { m_LCRFuncType = static_cast<LCRFuncType>(type); }
	void setLCRFuncType(LCRFuncType type) { m_LCRFuncType = type; }
	void recordHeaderToFile();
	void recordQAHeaderFile();
	void readSingleCurrent(float &Imeas); // I = current
	void readSingleCorZ(float &Zmeas, float &theta_meas, LCRFuncType funcType); // Zmeas and theta_meas are for Z and thetha if the third argument is LCRFuncType::Z_theta. Those are for C and D-factor if LCRFuncType::Cp_D or Cs_D.
	void clearData();
	void HVON();
	void HVOFF();
	bool setCurrentActiveDevice(std::string psname, std::string lcrname);
	bool setCurrentActiveDevice(std::string psname_HV, std::string psname_Vtest, std::string lcrname_1, std::string lcrname_2);
	bool setCurrentActiveDeviceQA(std::string psname_HV, std::string psname_Vtest, std::string lcrname_1, std::string lcrname_2);
	int stopRun();
	int runIVCVScan(bool runIV, bool runCV);
	int runCFScan();
	std::map<power_supply*, std::string>* getPS() { return m_powerSupplySetting_Map; }
	std::map<LCR_meter*, std::string>* getLCR() { return m_LCRMeterSetting_Map; }

	/*
	void SetTestV(std::string psname);
	void SetHV(std::string psname);
	void SetCint(std::string lcrname);
	void SetCcp(std::string lcrname);
*/

};

