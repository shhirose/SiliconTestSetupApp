#pragma once
#include "stdafx.h"
#include "pch.h"
#include "SiliconTestSetup.h"
#include <msclr/marshal_cppstd.h>
//#include "StripQAControlGUI.h"


SiliconTestSetup::SiliconTestSetup() {
	m_isGPIBOpen = false;
	m_vol1 = new std::vector<double>();
	m_curr = new std::vector<double>();
	m_curr2 = new std::vector<double>();
	m_curr3 = new std::vector<double>();
	m_res = new std::vector<double>();
	m_vol2 = new std::vector<double>();
	m_cap  = new std::vector<double>();
	m_deg  = new std::vector<double>();
	m_freq = new std::vector<double>();
	m_activePS_name      = new std::string();
	m_activePSHV_name    = new std::string();
	m_activePSVtest_name = new std::string();
	m_activeLCR_name     = new std::string();
	m_activeLCR1_name    = new std::string();
	m_activeLCR2_name    = new std::string();
	m_LCRFuncType = LCRFuncType::NOT_SET;
}
int SiliconTestSetup::initialize() {
	//---Making serial interface for devices---
	m_serialInterface = new prologix_gpibusb();
	std::string device_name = DEVICE_NAME;
	m_serialInterface->set_device_name(device_name);
	m_serialInterface->initialize();
	if (!m_serialInterface->is_initialized()) {
		//MSG_ERROR(" [ERROR] failed to create an instance of the serial interface for " << device_name << ".");
		return -1;
	}
	m_dataHandler = new DataHandler();

//	m_teslaProbeCtrl = new TeslaProbeCtrl();
//	m_teslaProbeCtrl->Initialize(22);
//	m_teslaProbeCtrl->SetSerialInterface(m_serialInterface);

	m_usbpio = new TUSBPIOCtrl(0);
	m_isGPIBOpen = true;

	m_ivcv = new IVCVCtrl();
	m_powerSupplySetting_Map = new std::map<power_supply*, std::string>();
	m_LCRMeterSetting_Map = new std::map<LCR_meter*, std::string>();
	m_powerSupplySetting_Map->clear();
	m_LCRMeterSetting_Map->clear();
	m_doneIV = false;
	m_doneCV = false;
	m_doneCF = false;

	return 0;
}

/******************* Prepare for the measurement *******************/

bool SiliconTestSetup::setCurrentActiveDevice(std::string psname, std::string lcrname) {
	// Dummy device names & addresses for psVtest and LCR2 
	return setCurrentActiveDevice(psname, "NoDevice:-1", lcrname, "NoDevice:-1");
}
bool SiliconTestSetup::setCurrentActiveDevice(std::string psname_HV, std::string psname_Vtest, std::string lcrname_1, std::string lcrname_2) {
	// Following is the set of information for power supply
	//  - For strip QA,
	//    -- psHV:    power supply for V_HV
	//    -- psVtest: power supply for V_test
	//  - For simple measurement
	//    -- psHV:    power supply
	//    -- psVtest: no device ("NoDevice:-1" is assumed)
	std::string psHV_deviceName    = psname_HV.substr(0, psname_HV.find(":"));
	std::string psVtest_deviceName = psname_Vtest.substr(0, psname_Vtest.find(":"));
	int psHV_deviceAddr    = atoi(psname_HV.substr(psname_HV.find(":") + 1).c_str());
	int psVtest_deviceAddr = atoi(psname_Vtest.substr(psname_Vtest.find(":") + 1).c_str());

	// Following is the set of information for LCR meter
	//  - For strip QA,
	//    -- lcr1: 1st LCR meter
	//    -- lcr2: 2nd LCR meter
	//  - For simple measurement
	//    -- lcr1: LCR meter
	//    -- lcr2: no device ("NoDevice:-1" is assumed)
	std::string lcr1_deviceName = lcrname_1.substr(0, lcrname_1.find(":"));
	std::string lcr2_deviceName = lcrname_2.substr(0, lcrname_2.find(":"));
	int lcr1_deviceAddr = atoi(lcrname_1.substr(lcrname_1.find(":") + 1).c_str());
	int lcr2_deviceAddr = atoi(lcrname_2.substr(lcrname_2.find(":") + 1).c_str());

	bool isStripQA = true;
	if (psVtest_deviceName == "NoDevice") isStripQA = false;

	MSG_INFO("Start to set PS now...");

	for (auto powerSupplySetting : *m_powerSupplySetting_Map) {
		std::string this_deviceName = powerSupplySetting.second;
		int this_deviceAddr = powerSupplySetting.first->get_address();

		// Check if this PS is equal to ps_HV (for strip QA) or ps (for simple measurement)
		if (this_deviceName == psHV_deviceName && this_deviceAddr == psHV_deviceAddr) {
			if (isStripQA) {
				MSG_INFO("PS(HV): " << this_deviceAddr << " " << this_deviceName);
				m_activePSHV = powerSupplySetting.first; // power_supply
				*m_activePSHV_name = powerSupplySetting.second; // std::string
			}
			else {
				MSG_INFO("PS: " << this_deviceAddr << " " << this_deviceName);
				m_activePS = powerSupplySetting.first; // power_supply
				*m_activePS_name = powerSupplySetting.second; // std::string
			}
			break;
		}
		if (this_deviceName == psVtest_deviceName && this_deviceAddr == psVtest_deviceAddr && isStripQA) {
			MSG_INFO("PS(Vtest): " << this_deviceAddr << " " << this_deviceName);
			m_activePSVtest = powerSupplySetting.first; // power_supply
			*m_activePSVtest_name = powerSupplySetting.second; // std::string
			break;
		}
	}

	MSG_INFO("Start to set LCR meter now...");
	for (auto LCRMeterSetting : *m_LCRMeterSetting_Map) {
		std::string this_deviceName = LCRMeterSetting.second;
		int this_deviceAddr = LCRMeterSetting.first->get_address();
		if (this_deviceName == lcr1_deviceName && this_deviceAddr == lcr1_deviceAddr) {
			if (isStripQA) {
				MSG_INFO("LCR(1) : " << this_deviceAddr << " " << this_deviceName);
				m_activeLCR1 = LCRMeterSetting.first;
				*m_activeLCR1_name = LCRMeterSetting.second;
			}
			else {
				MSG_INFO("LCR : " << this_deviceAddr << " " << this_deviceName);
				m_activeLCR = LCRMeterSetting.first;
				*m_activeLCR_name = LCRMeterSetting.second;
			}
			break;
		}
		if (this_deviceName == lcr2_deviceName && this_deviceAddr == lcr2_deviceAddr && isStripQA) {
			MSG_INFO("LCR(2) : " << this_deviceAddr << " " << this_deviceName);
			m_activeLCR2 = LCRMeterSetting.first;
			*m_activeLCR2_name = LCRMeterSetting.second;
			break;
		}
	}

	// Check if all PS and LCR meters are set as expected.
	bool isDevicesAreSet = false;
	if (isStripQA && m_activePSHV && m_activePSVtest) isDevicesAreSet = true;
	else if (!isStripQA && m_activePS) isDevicesAreSet = true;
	
	return isDevicesAreSet;

}

bool SiliconTestSetup::setCurrentActiveDeviceQA(std::string psname_HV, std::string psname_Vtest, std::string lcrname_1, std::string lcrname_2) {
	return setCurrentActiveDevice(psname_HV, psname_Vtest, lcrname_1, lcrname_2);
}

void SiliconTestSetup::clearData() {
	m_vol1->clear();
	m_curr->clear();
	m_vol2->clear();
	m_res->clear();
	m_curr2->clear();
	m_curr3->clear();
	m_cap->clear();
	m_deg->clear();
	m_freq->clear();
}

void SiliconTestSetup::doCalibration(bool isOpen) {
	if (!m_activeLCR && !m_activeLCR1) {
		MSG_WARNING("No LCR meter is set.");
		return;
	}

	if (m_activeLCR) {
		if (isOpen) m_activeLCR->zeroopen(m_serialInterface);
		else        m_activeLCR->zeroshort(m_serialInterface);
	}
	if (m_activeLCR1) {
		if (isOpen) m_activeLCR1->zeroopen(m_serialInterface);
		else        m_activeLCR1->zeroshort(m_serialInterface);
	}
	Sleep(6000);
	if (m_activeLCR2) {
		if (isOpen) m_activeLCR2->zeroopen(m_serialInterface);
		else        m_activeLCR2->zeroshort(m_serialInterface);
	}
}

void  SiliconTestSetup::doOpenCalibration() {
	doCalibration(true);
	MSG_INFO("Zero open calibration is done.");
}
void SiliconTestSetup::doShortCalibration() {
	doCalibration(false);
	MSG_INFO("Zero short calibration done.");
}

/******************* Basic functions to control PS / LCR meters *******************/

std::string SiliconTestSetup::getCurrentTime() {
	DateTime dt = DateTime::Now;
	//	return msclr::interop::marshal_as<std::string>(dt.ToString(L"r"));
	return msclr::interop::marshal_as<std::string>(dt.ToString(L"dd-MM-yyyy HH:mm:ss"));
}

void SiliconTestSetup::turnPSOn(power_supply* PS, float Ilimit) {
	turnPSOn(PS, Ilimit, false);
}

void SiliconTestSetup::turnPSOn(power_supply* PS, float Ilimit, bool setVrange) {
	PS->reset(m_serialInterface);
	PS->configure(m_serialInterface);
	//if (setVrange) PS->setvoltrange(m_serialInterface, m_Vend_HV);
	if (setVrange) PS->setvoltrange(m_serialInterface, m_Vend);
	PS->set_compliance(Ilimit);
	PS->config_compliance(m_serialInterface);
	PS->set_voltage(0.0);
	PS->config_voltage(m_serialInterface);
	PS->power_on(m_serialInterface);
}

void SiliconTestSetup::rampPSVoltage(power_supply *PS, float Vset, int Twait){
	MSG_INFO("Setting voltage: " << Vset);
	PS->set_voltage(Vset);
	MSG_DEBUG("Set voltage done.");
	PS->set_sweep_steps(1);
	MSG_DEBUG("Set sweep_steps done.");
	PS->set_sweep_sleep_in_ms(m_Tramp);
	MSG_DEBUG("Set sweep_steps_in_ms done.");
	PS->voltage_sweep(m_serialInterface);
	MSG_DEBUG("voltage_sweep done. then wait for " << Twait << " seconds...");
	Sleep(Twait);
}

void SiliconTestSetup::getPSCurrent(power_supply *PS, float &Imeas){
	MSG_DEBUG("read_current");
	Imeas = PS->read_current(m_serialInterface);
	MSG_DEBUG("Measured current = " << Imeas);
}

void SiliconTestSetup::stopPS(power_supply* PS) {
	MSG_INFO("Turn off PS (t_ramp = " << m_Tramp << " ms)");
	int rampStep = 1;
	if (m_Vfixed != 0 && abs(m_Vfixed) / 5 > 1) rampStep = abs(m_Vfixed) / 5;
	else if (m_Vend != 0 && abs(m_Vend) / 10 > 1) rampStep = abs(m_Vend) / 10;
	if(m_Vend_HV != 0 && abs(m_Vend_HV) / 10 > 1) rampStep = abs(m_Vend_HV) / 10;
	PS->set_sweep_steps(rampStep);
	//PS->set_sweep_sleep_in_ms(m_Tramp);
	PS->set_sweep_sleep_in_ms(2000);
	PS->set_voltage(0, m_Vfixed);
	PS->voltage_sweep(m_serialInterface);
	PS->power_off(m_serialInterface);
	MSG_INFO("This PS has been turned off.");
}

/*
void SiliconTestSetup::setLCR(LCR_meter* LCR) {
	LCR->reset(m_serialInterface);
	LCR->configure(m_serialInterface);
	LCR->set_displayfunc(m_serialInterface, static_cast<int>(m_LCRFuncType)); // Maybe not needed???
}
*/

void SiliconTestSetup::resetLCR(LCR_meter* LCR) {
	LCR->reset(m_serialInterface);
	LCR->configure(m_serialInterface);
}

void SiliconTestSetup::resetLCR(LCR_meter* LCR, LCRFuncType funcType) {
	resetLCR(LCR);
	// LCRFuncType = 0: Cp-D measurement
	// LCRFuncType = 1: Z-theta measurement
	// LCRFuncType = 2: Cs-D measurement
	MSG_DEBUG("Current value is " << static_cast<int>(m_LCRFuncType) << " vs required value is " << static_cast<int>(funcType));

	if (funcType != m_LCRFuncType) {
		m_LCRFuncType = funcType;
		LCR->set_displayfunc(m_serialInterface, static_cast<int>(m_LCRFuncType));
		Sleep(m_interval * 1000);
	}
	MSG_INFO("LCR meter is set to be " << static_cast<int>(m_LCRFuncType));
}

void SiliconTestSetup::changeLCRFrequency(LCR_meter* LCR, float Fset, int Twait) {
	LCR->set_frequency(Fset);
	LCR->config_frequency(m_serialInterface);
	Sleep(Twait);
	MSG_INFO("Frequency is set to be " << Fset << " [kHz]");
}

void SiliconTestSetup::getLCRValues(LCR_meter* LCR, float &C_or_Z, float &D_or_theta) {
	double val1_tmp = 0.0;
	double val2_tmp = 0.0;
	LCR->read_capacitance_and_degree(m_serialInterface, val1_tmp, val2_tmp);
	C_or_Z     = (float)val1_tmp;
	D_or_theta = (float)val2_tmp;
}

void SiliconTestSetup::stopLCR(LCR_meter* LCR) {
	// Do the same procedure as setLCR
	resetLCR(LCR);
}

/******************* Operation for the measurement *******************/

void SiliconTestSetup::recordHeaderToFile() {
	MSG_INFO("Writing header to file : " << m_dataHandler->GetFullDataFilename());
	std::ofstream ofscon(m_dataHandler->GetFullDataFilename().c_str(), std::ofstream::out);
	ofscon << "==========================" << std::endl;
	ofscon << "Device : " << std::endl;
	if (m_activePS) ofscon << "    " << (*m_activePS_name) << " " << m_activePS->get_address() << std::endl;
	if (m_activeLCR ) ofscon << "    " << (*m_activeLCR_name) << " " << m_activeLCR->get_address() << std::endl;
	ofscon << "Measurement type " << std::endl;
	ofscon << "    (runIV, runCV, runCF) = (" << m_doneIV << ", " << m_doneCV <<", " << m_doneCF << ")" << std::endl;
	ofscon << "    LCRtype : " << (m_LCRFuncType == LCRFuncType::Cp_D ? "Cp-D" : "Z-theta(dig)")  << "  if LCR meter used..."<< std::endl;
	ofscon << "==========================" << std::endl;
	ofscon << "[Scan Parameters] " << std::endl;
	ofscon << "Negative voltage  ? " << m_isNegativeVoltage << " " << std::endl;
	ofscon << "Current limet     : " << m_currentLimit << " uA " << std::endl;
	ofscon << "Start Voltage     : " << m_Vstart << " V" << std::endl;
	ofscon << "End Voltage       : " << m_Vend_HV << " V" << std::endl;
	ofscon << "Voltage Step      : " << m_Vstep << " V" << std::endl;
	ofscon << "Interval          : " << m_interval << " s " << std::endl;
	ofscon << "Ramping Time      : " << m_Tramp << " ms" << std::endl;
	ofscon << "Frequency (fixed) : " << m_F << " kHz" << std::endl;
	ofscon << "Start Freqequency : " << m_Fstart << " kHz" << std::endl;
	ofscon << "End Frequency     : " << m_Fend << " kHz" << std::endl;
	ofscon << "Voltage (fixed)   : " << m_Vfixed << " V" << std::endl;

	ofscon.close();
}

void  SiliconTestSetup::readSingleCurrent(float& Imeas) {
	power_supply* thisPS = nullptr;
	if (m_activePSVtest) {
		thisPS = m_activePSVtest;
	}
	else if (m_activePS) {
		thisPS = m_activePS;
	}
	else {
		MSG_ERROR("To supply fixed voltage please set power supply device.");
		return;
	}
	turnPSOn(thisPS, m_currentLimit * uA);
	rampPSVoltage(thisPS, m_Vsingle, 3000);
	getPSCurrent(thisPS, Imeas);
}

void  SiliconTestSetup::readSingleCorZ(float& Zmeas, float& theta_meas, LCRFuncType funcType) {
	if (!m_activeLCR) {
		if (!m_activeLCR1) {
			MSG_ERROR("LCR meter device is not set.");
			return;
		}
	}
	if (m_activeLCR) {
		resetLCR(m_activeLCR, funcType);
		changeLCRFrequency(m_activeLCR, m_Fsingle * kHz, m_interval);
	}
	if (m_activeLCR1) {
		if (m_QAparam_testTypeIndex == TestTypeIdentifier::CCPL_C) {
			resetLCR(m_activeLCR1, funcType);
			changeLCRFrequency(m_activeLCR1, m_Fsingle * kHz, m_interval);
		}
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_LOW_C) {
			resetLCR(m_activeLCR2, funcType);
			changeLCRFrequency(m_activeLCR2, m_Fsingle * kHz, m_interval);

		}
	}

	Sleep(m_interval);

	if (m_activeLCR) {
		getLCRValues(m_activeLCR, Zmeas, theta_meas);
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CCPL_C) {
		getLCRValues(m_activeLCR1, Zmeas, theta_meas);
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_LOW_C) {
		getLCRValues(m_activeLCR2, Zmeas, theta_meas);
	}
}

int SiliconTestSetup::stopRun() {
	MSG_INFO("==== Scan terminated by Stop ==== ");
	MSG_INFO("Ramping down...");

	TUSBPIOCtrl usbpio(0);
	usbpio.SetSWoff();

	if (m_activePS) stopPS(m_activePS);
	if (m_activePSVtest) stopPS(m_activePSVtest);
	if (m_activePSHV && (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_LOW_C || m_QAparam_testTypeIndex == TestTypeIdentifier::INT_MID_IV || m_QAparam_testTypeIndex == TestTypeIdentifier::PTP_IV)) {
		stopPS(m_activePSHV);
	}
	if (m_activeLCR)  stopLCR(m_activeLCR);
	if (m_activeLCR1) stopLCR(m_activeLCR1);
	if (m_activeLCR2) stopLCR(m_activeLCR2);

	MSG_INFO("==== Scan finished ==== ");
	return 0;
}

int SiliconTestSetup::runCFScan() {
	if (!m_activeLCR) {
		MSG_ERROR("LCR meter device is not set.");
		return -1;
	}
	if (!m_activePS) {
		MSG_ERROR("To supply fixed voltage please set power supply device.");
		return -2;
	}
	if (m_Vfixed != 0) {
		MSG_WARNING("Invalid value of V: (" << m_Vfixed << "); the value must not be equal to zero.");
		return -3;
	}

	float Imeas = 0;
	turnPSOn(m_activePS, m_currentLimit * uA);
	rampPSVoltage(m_activePS, m_isNegativeVoltage ? -1 * fabs(m_Vfixed) : fabs(m_Vfixed), 3000);
	getPSCurrent(m_activePS, Imeas);
	MSG_INFO("Current value : " << Imeas);
	
	std::ofstream ofscon(m_dataHandler->GetFullDataFilename().c_str(), std::ofstream::app);
	ofscon << "==========================" << std::endl;
	ofscon << "[pre-set voltage] " << std::endl;
	ofscon << "fixed voltage : " << m_Vfixed << std::endl;
	ofscon << "current value : " << Imeas << std::endl;
	ofscon.close();
	
	// Done the current meausrement; next is to start CF scan.

	double unit = 1;
	if (m_LCRFuncType == LCRFuncType::Cp_D)unit = pF;
	else if (m_LCRFuncType == LCRFuncType::Z_theta) unit = kOhm;

	resetLCR(m_activeLCR); // Need to check the behaviour!!!

	recordHeaderToFile();

	ofscon.open(m_dataHandler->GetFullDataFilename().c_str(), std::ofstream::app);
	ofscon << "==========================" << std::endl;
	ofscon << "[Data Format] " << std::endl;
	if (m_LCRFuncType == LCRFuncType::Cp_D)ofscon << "Frequency[Hz] capacitance[F] D time(dd-MM-yyyy HH:mm:ss)" << std::endl;
	else ofscon << "Frequency[Hz] impedance[Ohm] Phase[deg] time(dd-MM-yyyy HH:mm:ss)" << std::endl;

	int Fstep = 1;
	int this_F = m_Fstart*kHz;
	while (this_F <= m_Fend*kHz) {
		if (this_F > 0 && this_F < 1e2) Fstep = 5;
		else if (this_F >= 1e2 && this_F <  1e3) Fstep = 100;
		else if (this_F >= 1e3 && this_F <  1e4) Fstep = 1000;
		else if (this_F >= 1e4 && this_F <  1e5) Fstep = 10000;
		else if (this_F >= 1e5 && this_F <= 1e6) Fstep = 100000;
		else if (this_F > 1e6) break;

		float this_C = -1;
		float this_Dfactor = -1;
		changeLCRFrequency(m_activeLCR, this_F, m_interval);
		MSG_INFO(this_F / kHz << " [kHz]");
		resetLCR(m_activeLCR, LCRFuncType::Cs_D);
		getLCRValues(m_activeLCR, this_C, this_Dfactor);

		// Do we need the following lines?
		m_freq->push_back(this_F/kHz);
		m_cap->push_back(this_C / unit);
		m_deg->push_back(this_Dfactor);

		std::string currentTime = getCurrentTime();
		ofscon << this_F << " " << this_C << " " << this_Dfactor << " " << currentTime << std::endl;
		MSG_INFO(this_F << " " << this_C << " " << this_Dfactor << " " << currentTime);

		this_F += Fstep;
	}
	if (m_Vfixed != 0) {
		MSG_INFO("Ramping down");
		stopPS(m_activePS);
	}
	ofscon.close();
	stopLCR(m_activeLCR);

	MSG_INFO("==== Scan finished ==== ");
	m_doneCF = true;

	return 0;
}

int SiliconTestSetup::runIVCVScan(bool runIV, bool runCV) {

	if (runIV && !m_activePS && !m_activePSVtest) {
		MSG_ERROR("Error! power supply is not set though IV scan is to be run.");
		return -1;
	}
	if (runCV && !m_activeLCR && !m_activeLCR1 && !m_activeLCR2) {
		MSG_ERROR("Error! LCR meter device is not set though CV scan is to be run.");
		return -2;
	}
	if (!runIV && !runCV) {
		MSG_WARNING("Nothing will happen because no device is set.");
		return -1;
	}
	MSG_INFO("Setting active devices.");
	if (runIV && m_activePS) {
		turnPSOn(m_activePS, m_currentLimit * uA, m_Vend);
		recordHeaderToFile();
	}
	if (runIV||runCV && m_activePSVtest) {
		turnPSOn(m_activePSVtest, m_currentLimit * uA, m_Vend);
	}

	if (m_activeLCR) {
		resetLCR(m_activeLCR);
		changeLCRFrequency(m_activeLCR, m_F * kHz, 1000);
		recordHeaderToFile();
	}

	if (m_activeLCR1) {
		resetLCR(m_activeLCR1);
		if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV) changeLCRFrequency(m_activeLCR1, m_F / 10 * kHz, 1000);
		else                              changeLCRFrequency(m_activeLCR1, m_F * kHz, 1000);
	}
	if (m_activeLCR2) {
		resetLCR(m_activeLCR2);
		if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV) changeLCRFrequency(m_activeLCR2, m_F * kHz / 10, 1000);
		else                              changeLCRFrequency(m_activeLCR2, m_F * kHz, 1000);
	}
	MSG_INFO("Setting active devices done...");
	
	std::ofstream ofscon(m_dataHandler->GetFullDataFilename().c_str(), std::ofstream::app);
	if      (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV)   ofscon << "Voltage[V] Current_R1[A] Current_R2[A] Current_R3[A]" << std::endl;
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_MID_IV) ofscon << "Voltage[V]  Current[A]" << std::endl;
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::PTP_IV)     ofscon << "Voltage[V]  Itest4[A]" << std::endl;
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CCPL_IV_10) ofscon << "Vcoupl[V]  Icoupl[A]" << std::endl;
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV)    ofscon << "Voltage[V] Capacitance[F] Resistance[Ohm]" << std::endl;
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV)   ofscon << "Voltage[V] Current[A] Capacitance[F] Resistance[Ohm]" << std::endl;
	else {
		ofscon << "==========================" << std::endl;
		ofscon << "[Data Format] " << std::endl;
		if (m_LCRFuncType == LCRFuncType::Cp_D) ofscon << "voltage[V] current[A] capacitance[F] D time(dd-MM-yyyy HH:mm:ss)" << std::endl;
		else                    ofscon << "voltage[V] current[A] impedance[Ohm] Phase[deg] time(dd-MM-yyyy HH:mm:ss)" << std::endl;
	}

	// Start Vscan
	m_doneIV = runIV;
	double thisvolt = m_Vstart * -1;
	bool isCurrentLimitExceeded = false;
	double volt;

	float Cmeas = -1.;
	float Dfactor_meas =-999.9;
	float Zmeas = 999.9;
	float theta_meas = -500.0;

	
	
	while (thisvolt <= m_Vend +0.1) {
		MSG_INFO("== Set voltage to " << thisvolt << "V ==");
		volt = 0;
		if      (m_activePS)                   volt = m_isNegativeVoltage ? -1 * thisvolt : thisvolt;
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::PTP_IV)  volt = m_isNegativeVoltage ? -1 * thisvolt : thisvolt;
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV) volt = m_isNegativeVoltage ? -1 * thisvolt : thisvolt;
		else                                   volt = thisvolt;
		
		float Imeas_Rbias[3] = { -1., -1., -1. };
		if ((runIV || runCV) && m_activePS) {
			rampPSVoltage(m_activePS, volt, m_interval * 1000);
			getPSCurrent(m_activePS, Imeas_Rbias[0]);

			if (Imeas_Rbias[0] == 0) {
				Imeas_Rbias[0] = 1e-6 * uA;
				MSG_WARNING("Current value is 0... assigned 1e-6 uA");
			}
			MSG_INFO("(volt,curr1)=(" << volt << ", " << Imeas_Rbias[0] << ")");

			if (runIV) { // Do we need the following lines?
				m_vol1->push_back(volt);
				m_curr->push_back(Imeas_Rbias[0] / uA);
			}
			if (abs(Imeas_Rbias[0]) > abs(m_currentLimit * uA)) isCurrentLimitExceeded = true;
		}

		if (m_activePSVtest) {
			TUSBPIOCtrl usbpio(0);
			if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV) {
				usbpio.SetSWoff();
				Tusbpio_Dev1_Write(0, 2, 0x4);
				Tusbpio_Dev1_Write(0, 1, 0x40);
				usbpio.SetSWon(0);
			}
			//else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_MID_IV) {
			//	Tusbpio_Dev1_Write(0, 2, 0x2);
			//	Tusbpio_Dev1_Write(0, 1, 0x90);
			//	Tusbpio_Dev1_Write(0, 0, 0x8);
			//	usbpio.SetSWon(0);
			//}

			if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV) rampPSVoltage(m_activePSVtest, -1 * volt, m_interval * 1000);
			else                                                         rampPSVoltage(m_activePSVtest,      volt, m_interval * 1000);
			getPSCurrent(m_activePSVtest, Imeas_Rbias[0]);
			std::cout <<"m_curr=" << m_curr->size() << std::endl;
			if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV) {
				// Start the second Rbias
				usbpio.SetSWoff();
				Tusbpio_Dev1_Write(0, 2, 0x4);
				Tusbpio_Dev1_Write(0, 1, 0x80);
				usbpio.SetSWon(0);
				Sleep(1000);
				getPSCurrent(m_activePSVtest, Imeas_Rbias[1]);
				m_curr2->push_back(Imeas_Rbias[1] / uA);
				std::cout << "m_curr2=" << m_curr2 << std::endl;
				// Start the last Rbias
				usbpio.SetSWoff();
				Tusbpio_Dev1_Write(0, 2, 0x4);
				Tusbpio_Dev1_Write(0, 1, 0x10);
				usbpio.SetSWon(0);
				Sleep(1000);
				getPSCurrent(m_activePSVtest, Imeas_Rbias[2]);
				m_curr3->push_back(Imeas_Rbias[2] / uA);
				std::cout << "m_curr3=" << m_curr3 << std::endl;
			}

			if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV) {
				MSG_INFO("(volt,curr1,curr2,curr3)=(" << volt << ", " << Imeas_Rbias[0] << ", " << Imeas_Rbias[1] << ", " << Imeas_Rbias[2] << ")");
			}
			else {
				MSG_INFO("(volt,curr)=(" << volt << ", " << Imeas_Rbias[0] << ")");
			}

			if (runIV) { // Do we need the following lines?
				m_vol1->push_back(volt);
				m_curr->push_back(Imeas_Rbias[0] / uA);
			}
			if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV){
				if (abs(Imeas_Rbias[0]) > abs(m_currentLimit * uA) || abs(Imeas_Rbias[1]) > abs(m_currentLimit * uA) || abs(Imeas_Rbias[2]) > abs(m_currentLimit * uA)) {
					MSG_INFO("Current limit exceeded: (limit = " << m_currentLimit * uA << " vs measured = " << Imeas_Rbias[0] * uA << ")");
					isCurrentLimitExceeded = true;
				}
			}
			else if (abs(Imeas_Rbias[0]) > abs(m_currentLimit * uA)) {
				MSG_INFO("Current limit exceeded: (limit = " << m_currentLimit * uA << " vs measured = " << Imeas_Rbias[0] * uA << ")");
				isCurrentLimitExceeded = true;
			}
		}
		if (isCurrentLimitExceeded) break;
		Sleep(1000);

		
		
		
		
		if (runCV) {
			m_doneCV = runCV;
			if (m_activeLCR) {
				resetLCR(m_activeLCR2, LCRFuncType::Cp_D);
				getLCRValues(m_activeLCR, Cmeas, Dfactor_meas);
				double unit = pF;
				if      (m_LCRFuncType == LCRFuncType::Cp_D)    unit = pF;
				else if (m_LCRFuncType == LCRFuncType::Z_theta) unit = kOhm;
				MSG_INFO("(cap,deg)=(" << Cmeas << ", " << Dfactor_meas << ")");

				// Do we need the following lines?
				m_vol2->push_back(fabs(volt));
				m_cap->push_back(Cmeas / unit);
				if (!m_doneIV) m_deg->push_back(Dfactor_meas);
			}
			if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV) {
				resetLCR(m_activeLCR2, LCRFuncType::Cp_D);
				getLCRValues(m_activeLCR2, Cmeas, Dfactor_meas);
				MSG_INFO("(C, D)=(" << Cmeas << ", " << Dfactor_meas << ")");
				resetLCR(m_activeLCR2, LCRFuncType::Z_theta);
				getLCRValues(m_activeLCR2, Zmeas, theta_meas);
				MSG_INFO("(Z, theta)=( " << Zmeas << " , " << theta_meas << " )");

				// Do we need the following lines?
				double unit = pF;
				if      (m_LCRFuncType == LCRFuncType::Cp_D)    unit = pF;
				else if (m_LCRFuncType == LCRFuncType::Z_theta) unit = kOhm;
				m_vol1->push_back(fabs(volt));
				m_cap->push_back(Cmeas / pF);
			}
			if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV) {
				resetLCR(m_activeLCR1, LCRFuncType::Cp_D);
				getLCRValues(m_activeLCR1, Cmeas, Dfactor_meas);
				MSG_INFO("(C, D)=(" << Cmeas << ", " << Dfactor_meas << ")");

				// Do we need the following lines?
				double unit = pF;
				if      (m_LCRFuncType == LCRFuncType::Cp_D)    unit = pF;
				else if (m_LCRFuncType == LCRFuncType::Z_theta) unit = kOhm;
				m_vol2->push_back(fabs(volt));
				m_cap->push_back(Cmeas / pF);
				
				
				
				if (getCapacitance().size() == 0) {
					m_cap->push_back(Cmeas / pF);
				}
				m_deg->push_back(Dfactor_meas);
				resetLCR(m_activeLCR1, LCRFuncType::Z_theta);
				getLCRValues(m_activeLCR1, Zmeas, theta_meas);
				MSG_INFO("(Z, theta)=( " << Zmeas << " , " << theta_meas << " )");

				// Do we need the following lines?
				/*
				unit = pF;
				if      (m_LCRFuncType == LCRFuncType::Cp_D)    unit = pF;
				else if (m_LCRFuncType == LCRFuncType::Z_theta) unit = kOhm;
				m_vol2->push_back(fabs(volt));
				m_cap->push_back(Zmeas / unit);
				m_deg->push_back(theta_meas);
				*/
				resetLCR(m_activeLCR1);
			}
		}
		std::cout << volt << std::endl;
		if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV) {
			ofscon << volt << " " << Imeas_Rbias[0] << " " << Imeas_Rbias[1] << " " << Imeas_Rbias[2] << std::endl;
			}
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_MID_IV) ofscon << volt << " " << Imeas_Rbias[0] << std::endl;
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::PTP_IV) { 
			ofscon << volt << " " << Imeas_Rbias[0] << std::endl;
			m_res->push_back(volt /( Imeas_Rbias[0]*1e6));
			}
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CCPL_IV_10) ofscon << volt << " " << Imeas_Rbias[0] << std::endl;
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV)    ofscon << -1 * volt << " " << Cmeas << " " << Zmeas << std::endl;
		else if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV)   ofscon << volt << " " << Imeas_Rbias[0] << " " << Cmeas << " " << Zmeas << std::endl;
		else {
			std::string currentTime = getCurrentTime();
			ofscon << volt << " " << Imeas_Rbias[0] << " " << Cmeas << " " << Dfactor_meas << " " << currentTime << std::endl;
			MSG_INFO(volt << " " << Imeas_Rbias[0] << " " << Cmeas << " " << Dfactor_meas << " " << currentTime);
		}

		thisvolt += m_Vstep;
		
		MSG_DEBUG("Going to next voltage...");
	}
	ofscon.close();
	MSG_DEBUG("Done.");
	stopRun();

	if (runCV && m_activeLCR) {
		stopLCR(m_activeLCR);
	}
	MSG_INFO("==== Scan finished ==== ");
	return 0;
}
void SiliconTestSetup::HVON(){
	turnPSOn(m_activePSHV, 1500 * uA);

	float thisvolt = 0;
	bool isCurrentLimitExceeded = false;
	while (thisvolt >= m_Vend_HV) {
		MSG_INFO("Set HV to  " << thisvolt << "V");
		rampPSVoltage(m_activePSHV, thisvolt, 3 * 1000);
		if (isCurrentLimitExceeded)break;
		thisvolt -= 10;
	}
	if (isCurrentLimitExceeded) HVOFF();
}
void SiliconTestSetup::HVOFF() {
	MSG_INFO("ramping down");
	m_Tramp = 10000;
	stopPS(m_activePSHV);
}
int SiliconTestSetup::removePS(std::string PSname, int addr) {
	std::map<power_supply*, std::string>::iterator it;
	for (it = m_powerSupplySetting_Map->begin(); it != m_powerSupplySetting_Map->end();) {
		if(it->first->get_address() == addr && it->second == PSname){
			it = m_powerSupplySetting_Map->erase(it);
		}
		else {
			it++;
		}
	}
	return 0;
}

int SiliconTestSetup::removeLCR(std::string LCRname, int addr) {
	std::map<LCR_meter*, std::string>::iterator it;
	for (it = m_LCRMeterSetting_Map->begin(); it != m_LCRMeterSetting_Map->end();) {
		if (it->first->get_address() == addr && it->second == LCRname) {
			it = m_LCRMeterSetting_Map->erase(it);
		}
		else {
			it++;
		}
	}
	return 0;
}
int SiliconTestSetup::setPS(std::string PSname, int addr) {
	power_supply* ps = nullptr;
	if      (PSname == "keithley2410")  ps = new keithley2410(addr);
	else if (PSname == "keithley6517A") ps = new keithley6517A(addr);
	else {
		MSG_ERROR("Invalid source meter: " << PSname << ".");
		return -1;
	}

	if (ps == nullptr) {
		MSG_ERROR("Error: source meter wasn't set.");
		return -1;
	}

	ps->set_voltage(0, 0); //initial voltage
	m_powerSupplySetting_Map->insert(std::make_pair(ps, PSname));

	return 0;
}
int SiliconTestSetup::setLCR(std::string LCRname, int addr) {
	std::map<LCR_meter*, std::string>::iterator it;
	for (it = m_LCRMeterSetting_Map->begin(); it != m_LCRMeterSetting_Map->end(); it++) {
		if (it->first->get_address() == addr && it->second == LCRname) {
			MSG_WARNING("Same device has been added already.");
			return -1;
		}
	}

	LCR_meter* lcr = nullptr;
	if      (LCRname == "hp4192A") lcr = new hp4192A(addr);
	else if (LCRname == "hp4284A") lcr = new hp4284A(addr);
	else {
		MSG_ERROR("Invalid LCR meter: " << LCRname << ".");
		return -1;
	}

	if (lcr == nullptr) {
		MSG_ERROR("Error: LCR meter wasn't set.");
		return -1;
	}
	m_LCRMeterSetting_Map->insert(std::make_pair(lcr, LCRname));

//	std::cout << m_LCRMeterSetting_Map->size() << std::endl;
	return 0;
}

int SiliconTestSetup::openTeslaT200Control() {
	char szCmd[256] = "mstsc /w:1500 /h:820 C:\\Users\\atlasj\\Desktop\\prober.rdp";
	MSG_INFO("Ppening Remote Desktop for Probe Station : " << szCmd);
	//	return openApplication(szCmd);
	return openApplication(szCmd);
}
int SiliconTestSetup::openApplication(char* szCmd) {
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOA si = { sizeof(STARTUPINFO) };
	int ii = CreateProcessA(NULL, szCmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi);
	return ii;
}
int SiliconTestSetup::createNewConsole(char* szCmd) {
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOA si = { sizeof(STARTUPINFO) };
	int ii = CreateProcessA(NULL, szCmd, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
	return ii;
}
void SiliconTestSetup::recordQAHeaderFile(){
	std::time_t t = std::time(0);   // get time now
	std::tm* now = std::localtime(&t);
	
	System::String^ batchNumber = m_QAparam_batchNumber;
	System::String^ waferNumber = m_QAparam_waferNumber;
	System::String^ waferType = m_QAparam_waferType;
	System::String^ DeviceType = m_QAparam_DeviceType;
	System::String^ DeviceNumber = m_QAparam_DeviceNumber;
	System::String^ label = m_QAparam_label;
	System::String^ testType = m_QAparam_testType;
	System::String^ runNumber = m_QAparam_runNumber;
	System::String^ serialNumber = m_QAparam_serialNumber;
	System::String^ operatorName = m_QAparam_operatorName;
	System::String^ temperature = m_QAparam_temperature;
	System::String^ humidity = m_QAparam_humidity;

	m_dataHandler->SetQAFilename(msclr::interop::marshal_as<std::string>(batchNumber), msclr::interop::marshal_as<std::string>(waferType),msclr::interop::marshal_as<std::string>(DeviceNumber), msclr::interop::marshal_as<std::string>(label), msclr::interop::marshal_as<std::string>(testType), msclr::interop::marshal_as<std::string>(runNumber));
	
	std::ofstream ofscon(m_dataHandler->GetFullDataFilename().c_str(), std::ofstream::out);
	ofscon << msclr::interop::marshal_as<std::string>(batchNumber) << "_" << msclr::interop::marshal_as<std::string>(waferType) << "_" << msclr::interop::marshal_as<std::string>(DeviceNumber) << "_" << msclr::interop::marshal_as<std::string>(label) << msclr::interop::marshal_as<std::string>(testType) << "_" << msclr::interop::marshal_as<std::string>(runNumber) << ".dat" << std::endl;
	ofscon << "Type: " << msclr::interop::marshal_as<std::string>(waferType) << std::endl;
	ofscon << "Batch: " << msclr::interop::marshal_as<std::string>(batchNumber) << std::endl;
	ofscon << "Wafer: " << msclr::interop::marshal_as<std::string>(waferNumber) << std::endl;
	ofscon << "Device Type: Testchip&MD8" << msclr::interop::marshal_as<std::string>(DeviceType) << std::endl;
	ofscon << "Label: " << msclr::interop::marshal_as<std::string>(label) << std::endl;
	ofscon << "Component Serial Number: "<< msclr::interop::marshal_as<std::string>(serialNumber) << std::endl;
	ofscon << "Date: "<< now->tm_mday<< "/" << (now->tm_mon + 1)<< "/"<< (now->tm_year + 1900) << std::endl;
	ofscon << "Time: " << now->tm_hour << ":"<< now->tm_min <<std::endl;
	ofscon << "Institute: KEK-Tsukuba" << std::endl;
	ofscon << "Operator: "<< msclr::interop::marshal_as<std::string>(operatorName) << std::endl;
	ofscon << "TestType: "<< msclr::interop::marshal_as<std::string>(testType) << std::endl;
	if (m_QAparam_testTypeIndex == TestTypeIdentifier::RBIAS_IV) {
		ofscon << "Instrument; " << "keithley2410" << std::endl;
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CCPL_C) {
		ofscon << "Instrument; " << "HP4284A" << std::endl;
		ofscon << "Frequency: " << m_Fsingle << " kHz" << std::endl;
		ofscon << "Circuit: CR-Parallel" << std::endl;
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::INT_LOW_C) {
		ofscon << "Instrument: " << "HP4284A" << std::endl;
		ofscon << "Frequency: " << m_Fsingle <<" kHz"<< std::endl;
		ofscon << "Circuit: CR-Parallel" << std::endl;
		ofscon << "VBIAS: " << m_Vend_HV <<" V"<< std::endl;
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::PTP_IV) {
		ofscon << "VBIAS: " << m_Vend_HV << " V" << std::endl;
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::CFLD_CV) {
		ofscon << "Instrument: HP4284A" << std::endl;
		ofscon << "Frequency: " << m_F << " kHz" << std::endl;
		ofscon << "Circuit: CR-Parallel" << std::endl;
	}
	else if (m_QAparam_testTypeIndex == TestTypeIdentifier::MD8_IVCV) {
		ofscon << "Instrument: HP4284A,Keithley2410" << std::endl;
		ofscon << "Frequency" << m_F << std::endl;
		ofscon << "Circuit: CR-Parallel" << std::endl;
	}
	ofscon << "RunNo: " << msclr::interop::marshal_as<std::string>(runNumber) << std::endl;
	ofscon << "Temperature: " << msclr::interop::marshal_as<std::string>(temperature) << " -C" << std::endl;
	ofscon << "Humidity: " << msclr::interop::marshal_as<std::string>(humidity)<<" %" << std::endl;
	ofscon << "Comments: "<<std::endl;
	ofscon.close();
}

void SiliconTestSetup::legacyRun() {
//	createNewConsole("");



//	m_teslaProbeCtrl->command_test();

	//	m_ivcv->test(m_serialInterface);
	m_ivcv->main_test(m_serialInterface);

	int PortN;
	MSG_INFO("Select Port No.");
	MSG_INFO("PortA=0, PortB=1, PortC=2");
	std::cin >> PortN;
	m_usbpio->SetBit(PortN, 0x4);
	m_usbpio->SetSWon(0);
	system("PAUSE");



}

