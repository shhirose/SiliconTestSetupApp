#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include<stdlib.h>
//#include"termiWin.h"
#include <stdio.h>
#include <tchar.h>
#include <iomanip>
#include "prologix_gpibusb.h"


class TeslaProbeCtrl
{
private:
	int m_address;
	serial_interface* si;
public:
	TeslaProbeCtrl();
	~TeslaProbeCtrl() { ; }
	int Initialize(int _addr = 22);
	void SetSerialInterface(serial_interface* _si) { si = _si; }
	void printdevice();
	int command_test();
};

