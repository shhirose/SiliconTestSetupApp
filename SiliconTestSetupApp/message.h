#pragma once

#define DEBUG_LEVEL 0

#define MSG_OUTPUT(lvl, flg, xmsg) if (lvl >= DEBUG_LEVEL) std::cout << xmsg << std::endl;

#define MSG_DEBUG(xmsg)   if (DEBUG_LEVEL <= 0) std::cout << "DEBUG: " << __func__ << ": " << xmsg << std::endl;
#define MSG_INFO(xmsg)    if (DEBUG_LEVEL <= 1) std::cout << "INFO: " << __func__ << ": " << xmsg << std::endl;
#define MSG_WARNING(xmsg) if (DEBUG_LEVEL <= 2) std::cout << "\033[33mWARNING: " << __func__ << ": " << xmsg << "\033[m" << std::endl;
#define MSG_ERROR(xmsg)   if (DEBUG_LEVEL <= 3) std::cout << "\033[31mERROR: " << __func__ << ": " << xmsg << "\033[m" << std::endl;

