#include "pch.h"
#include "TeslaProbeCtrl.h"

TeslaProbeCtrl::TeslaProbeCtrl() {

}
int TeslaProbeCtrl::Initialize(int _addr) {
	m_address = _addr;
	return 0;
}
void TeslaProbeCtrl::printdevice() {
    si->set_address(m_address);
    //	si->write(":SOURCE:VOLTAGE:LEVEL " + std::to_string(m_voltage) + "\r\n");
    std::string buffer = "";
    while (buffer.length() == 0) {
        si->write("*IDN?\r\n");
        si->write("++read\r\n");
        Sleep(100);
        si->make_talker();
        si->read(buffer);
        si->make_listener();
    }
    std::cout << buffer << std::endl;
}

int TeslaProbeCtrl::command_test() {
    printdevice();
    si->set_address(m_address);
    // absolute move
///    si->write(":move:absolute 2 10 10 none\r\n");

    // relative move
    si->write("MoveChuck -100 0 R Y 100\r\n");

    return 0;
}